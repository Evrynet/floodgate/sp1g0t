package main

import (
	"os"
	"os/signal"
	"strconv"

	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
	"gitlab.com/Evrynet/sp1g0t/aws"
	"gitlab.com/Evrynet/sp1g0t/bot"
	"gitlab.com/Evrynet/sp1g0t/cmd"
	"gitlab.com/Evrynet/sp1g0t/db"
	"gitlab.com/Evrynet/sp1g0t/db/migrations"
	"gitlab.com/Evrynet/sp1g0t/db/repository"
	"gitlab.com/Evrynet/sp1g0t/hydra"
	_ "gitlab.com/Evrynet/sp1g0t/logger"
	"gitlab.com/Evrynet/sp1g0t/providers"
	"gitlab.com/Evrynet/sp1g0t/providers/coingecko"
	"gitlab.com/Evrynet/sp1g0t/providers/gateio"
	"gitlab.com/Evrynet/sp1g0t/utils"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		logrus.Error("Error loading .env file")
	}

	loglvl, err := logrus.ParseLevel(os.Getenv("LOG_LEVEL"))
	if err != nil {
		loglvl = logrus.InfoLevel
	}
	logrus.SetLevel(loglvl)

	logrus.Debug("Begin connecting to db")
	gormDB, err := db.New(db.GetDBAuthFromEnv())
	if err != nil {
		logrus.Fatal(err)
	}
	logrus.Debug("Successfully connected to db")

	migrator := db.NewMigration(db.SqlDBFromGorm(gormDB))
	err = migrations.RunMigrations(migrator)
	if err != nil {
		logrus.Fatal(err)
	}

	logrus.Debug("Init hydraAPI..")
	h := hydra.NewHydra(utils.HTTP_CLIENT_DELAY, utils.TrimSpacesFromEnv(os.Getenv(utils.SOLANA_HOST)),
		utils.TrimSpacesFromEnv(os.Getenv(utils.SOLANA_PRIVATE_KEY)), utils.TrimSpacesFromEnv(os.Getenv(utils.WALLET)))
	logrus.Debug("hydraAPI initted successfully")

	logrus.Debug("Init coingecko..")
	coingeckoTimeout := os.Getenv(utils.COINGECKO_TIMEOUT)
	isTimeout, err := strconv.ParseBool(coingeckoTimeout)
	if err != nil {
		logrus.Fatal(err)
	}
	coingecko := coingecko.NewCoingecko(&coingecko.CoingeckoApi{}, isTimeout)
	logrus.Debug("Init gateio..")
	gateIO := gateio.NewGateIO(&gateio.GateeIOApi{})
	priceProvider := providers.NewPriceProvider(coingecko, gateIO)
	logrus.Debug("Price providers are initialized successfully")

	logrus.Debug("Init repos..")
	orderRepo := repository.NewOrderRepository(gormDB)
	settleRepo := repository.NewSettlesRepository(gormDB)
	priceRepo := repository.NewPricesRepository(gormDB)
	walletBalanceRepo := repository.NewWalletBalancesRepository(gormDB)
	repo := repository.NewRepository(orderRepo, settleRepo, priceRepo, walletBalanceRepo)
	logrus.Debug("Repos are initialized successfully")

	logrus.Debug("Init aws..")
	awsService := aws.New(aws.NewSes(), aws.NewSMTP())
	logrus.Debug("AWS are initialized successfully")

	svc := bot.NewSp1g0tBot(&h, nil, repo, priceProvider, awsService)

	signals := make(chan os.Signal, 1)
	signal.Notify(signals, os.Interrupt)

	go func() {
		<-signals
		signal.Stop(signals)
		logrus.Info("\nCTRL-C command received. Exiting...")
		os.Exit(0)
	}()

	cmd.Execute(svc)
}
