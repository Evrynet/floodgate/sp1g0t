package db

import (
	"database/sql"

	migrate "github.com/rubenv/sql-migrate"
	"github.com/sirupsen/logrus"
)

type Migration struct {
	db *sql.DB
}

func NewMigration(db *sql.DB) Migration {
	return Migration{
		db: db,
	}
}

// Migrate migrates the database to the latest schema
func (m *Migration) Migrate(migrations migrate.MigrationSource) (err error) {
	logrus.Debug("Applying database migrations...")
	n, err := migrate.Exec(m.db, "postgres", migrations, migrate.Up)
	if err != nil {
		logrus.Errorf("Database migrations failed: %s", err)

		// Try to roll back
		if n > 0 {
			logrus.Errorf("Rolling back %v migrations...", n)
			_, rerr := migrate.ExecMax(m.db, "postgres", migrations, migrate.Down, n)
			if rerr != nil {
				logrus.Errorf("Failed to roll back migrations: %s", rerr)
			} else {
				logrus.Errorf("Successfully rolled back %v migrations", n)
			}
		}
		return err
	}
	if n > 0 {
		logrus.Debugf("Successfully applied %v database migrations", n)
	} else {
		logrus.Debug("Database schema is already up-to-date")
	}
	return nil
}
