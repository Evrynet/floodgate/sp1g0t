package migrations

import (
	migrate "github.com/rubenv/sql-migrate"
	"github.com/sirupsen/logrus"
	"gitlab.com/Evrynet/sp1g0t/db"
)

// Migrations are defined in .go files in this package
// Each file must contain the definition for exactly one migration,
// which it appends to the migrations slice in an init() function.
var migrations []*migrate.Migration

func addMigration(id string, up []string, down []string) {
	migrations = append(migrations, &migrate.Migration{
		Id:   id,
		Up:   up,
		Down: down,
	})
}

func RunMigrations(migrator db.Migration) error {
	logrus.Debugf("Found %v migrations", len(migrations))
	migrations := &migrate.MemoryMigrationSource{Migrations: migrations}
	return migrator.Migrate(migrations)
}
