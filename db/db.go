package db

import (
	"database/sql"
	"errors"
	"time"

	"github.com/jpillora/backoff"
	"github.com/sirupsen/logrus"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"

	// Postgres database
	_ "github.com/lib/pq"
)

const maxAttemps = 10

// New creates a new database connections
func New(auth auth) (*gorm.DB, error) {
	psqlInfo := auth.databaseSourceName()

	b := &backoff.Backoff{
		Min:    1 * time.Second,
		Max:    30 * time.Second,
		Factor: 2,
	}
	var dbAttemp, gormAttemp float64
	for {
		if dbAttemp >= maxAttemps || gormAttemp >= maxAttemps {
			return nil, errors.New("failed to connect to the database. Max attempts exeded. Please check db connection")
		}
		db, err := sql.Open("postgres", psqlInfo)
		if err != nil {
			d := b.ForAttempt(dbAttemp)
			logrus.Infof("%s, reconnecting in %s", err, d)
			time.Sleep(d)
			dbAttemp++
			continue
		}

		gormDB, err := gorm.Open(postgres.New(postgres.Config{
			Conn: db,
		}), &gorm.Config{Logger: logger.Default.LogMode(logger.Silent)})
		if err != nil {
			d := b.ForAttempt(gormAttemp)
			logrus.Infof("%s, reconnecting in %s", err, d)
			time.Sleep(d)
			gormAttemp++
			continue
		}
		b.Reset()
		return gormDB, nil
	}
}

func SqlDBFromGorm(gormDB *gorm.DB) *sql.DB {
	db, _ := gormDB.DB()
	return db
}
