package repository

import (
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type WalletBalancesRepository struct {
	db *gorm.DB
}

func NewWalletBalancesRepository(db *gorm.DB) WalletBalancesRepository {
	return WalletBalancesRepository{db: db}
}

type WalletBalance struct {
	ID        uuid.UUID
	Asset     string    `json:"asset"`
	MintAddr  string    `json:"mint_addr"`
	Total     string    `json:"total"`
	CreatedAt time.Time `json:"created_at"`
}

func (w WalletBalancesRepository) SaveWalletBalance(walletBalance *WalletBalance) error {
	return w.db.Clauses(clause.OnConflict{DoNothing: true}).Create(&walletBalance).Error
}

func (w WalletBalancesRepository) GetWalletBalancesForPeriod(lossPeriod float64, dateNow time.Time) (walletBalances []WalletBalance, err error) {
	//TODO:
	//there is an issue probably with pgx lib or gorm itself - it cant parse 'interval', so for now using Raw queery is the only solution
	//should investigate the issue deeper later
	err = w.db.Raw(`
	SELECT * FROM wallet_balances 
	WHERE  (SELECT extract (epoch from $1::timestamp - created_at::timestamp)) <= $2::integer`, dateNow, lossPeriod).
		Scan(&walletBalances).Error

	return
}

func (w WalletBalancesRepository) GetWalletBalances() (walletBalances []WalletBalance, err error) {
	err = w.db.
		Find(&walletBalances).
		Error
	return
}
