package repository

import (
	"time"

	"gitlab.com/Evrynet/sp1g0t/models"
	"gitlab.com/Evrynet/sp1g0t/utils"
	"gorm.io/gorm"
)

type OrderRepository struct {
	db *gorm.DB
}

func NewOrderRepository(db *gorm.DB) OrderRepository {
	return OrderRepository{db: db}
}

type Order struct {
	models.OrderData
	CreatedAt  time.Time `json:"created_at"`
	ModifiedAt time.Time `json:"modified_at,omitempty"`

	TxID   string
	TxType string
}

func (o OrderRepository) SaveOrder(order Order) error {
	return o.db.Create(&order).Error
}

func (o OrderRepository) GetOrder(id string) (order *Order, err error) {
	err = o.db.Where("id = ?", id).Find(&order).Error
	return
}

func (o OrderRepository) CancelOrder(id, txID string) error {
	return o.db.Model(&Order{}).Where("id = ?", id).Update("status", utils.ORDER_STATUS_CANCELED).Update("tx_id", txID).Error
}

func (o OrderRepository) GetOrdersByStatus(status string) (orders []Order, err error) {
	err = o.db.Where("status = ?", status).Find(&orders).Error
	return
}

func (o OrderRepository) GetOwnOrdersForMarket(marketAddress string) (orders []Order, err error) {
	err = o.db.Model(&Order{}).Where("market_address = ? AND status = ?", marketAddress, utils.ORDER_STATUS_OPEN).Find(&orders).Error
	return
}
