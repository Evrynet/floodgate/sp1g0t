package repository

type Repository struct {
	Order         OrderRepository
	Settle        SettlesRepository
	Price         PricesRepository
	WalletBalance WalletBalancesRepository
}

func NewRepository(order OrderRepository, settle SettlesRepository, price PricesRepository, walletBalance WalletBalancesRepository) Repository {
	return Repository{
		Order:         order,
		Settle:        settle,
		Price:         price,
		WalletBalance: walletBalance,
	}
}
