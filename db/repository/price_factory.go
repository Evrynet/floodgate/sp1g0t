package repository

import (
	"context"
	"time"

	"github.com/bluele/factory-go/factory"
	"github.com/google/uuid"
	"gitlab.com/Evrynet/sp1g0t/testing_context"
)

var PriceFactory = factory.NewFactory(
	&AssetPrice{},
).Attr("ID", func(args factory.Args) (interface{}, error) {
	return uuid.New(), nil
}).OnCreate(func(args factory.Args) error {
	db := testing_context.GetDbFromContext(args.Context())
	return db.Create(args.Instance().(*AssetPrice)).Error
})

type priceBuilder map[string]interface{}

func GivenAPrice() priceBuilder {
	var pb = make(priceBuilder)
	return pb
}

func (pb priceBuilder) Build(ctx context.Context) *AssetPrice {
	var assetPrice = PriceFactory.MustCreateWithContextAndOption(ctx, pb).(*AssetPrice)
	return assetPrice
}

func (pb priceBuilder) Price(price string) priceBuilder {
	pb["Price"] = price
	return pb
}

func (pb priceBuilder) CreatedAt(date time.Time) priceBuilder {
	pb["CreatedAt"] = date
	return pb
}

func (pb priceBuilder) Asset(asset string) priceBuilder {
	pb["Asset"] = asset
	return pb
}
