package repository

import (
	"context"

	"github.com/bluele/factory-go/factory"
	"gitlab.com/Evrynet/sp1g0t/models"
	"gitlab.com/Evrynet/sp1g0t/testing_context"
)

var OrderFactory = factory.NewFactory(
	&Order{},
).SubFactory("OrderData", OrderDataFactory).
	OnCreate(func(args factory.Args) error {
		db := testing_context.GetDbFromContext(args.Context())
		return db.Create(args.Instance()).Error
	})

var OrderDataFactory = factory.NewFactory(
	models.OrderData{},
).Attr("Id", func(args factory.Args) (interface{}, error) {
	return "123123", nil
}).Attr("Price", func(args factory.Args) (interface{}, error) {
	return "2.1", nil
}).Attr("Size", func(args factory.Args) (interface{}, error) {
	return "0.1", nil
})

type orderBuilder map[string]interface{}

func GivenAnOrder() orderBuilder {
	var ob = make(orderBuilder)
	return ob
}

func (ob orderBuilder) Build(ctx context.Context) *Order {
	var order = OrderFactory.MustCreateWithContextAndOption(ctx, ob).(*Order)
	return order
}

func (ob orderBuilder) OrderData(od models.OrderData) orderBuilder {
	ob["OrderData"] = od
	return ob
}
