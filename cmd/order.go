package cmd

import (
	"fmt"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/Evrynet/sp1g0t/bot"
	"gitlab.com/Evrynet/sp1g0t/utils"
)

func newOrderCommand(b bot.Sp1g0tBot) *cobra.Command {
	orderCmd := &cobra.Command{
		Use:   "order",
		Short: "Support cmd for orders",
		Long:  `Support cmd for placing/canceling/getting order`,
		RunE:  executeOrderCommand(b),
	}

	orderCmd.Flags().BoolVar(&OrderFlags.Place, "place", false, "place order")
	orderCmd.Flags().StringVar(&OrderFlags.Pair, "pair", "", "Asset pair e.g. SOL/USDC")
	orderCmd.Flags().StringVar(&OrderFlags.Price, "price", "", "maximum price the client is willing to pay, will be ignored for market orders e.g. 0.01")
	orderCmd.Flags().StringVar(&OrderFlags.Side, "side", "", "trading side, may be one of: buy or sell")
	orderCmd.Flags().StringVar(&OrderFlags.Size, "size", "", "how much shall be bought or sold e.g. 0.01")
	orderCmd.Flags().StringVar(&OrderFlags.Type, "type", "", "Order type e.g. limit")

	orderCmd.Flags().StringVar(&OrderFlags.OrderID, "id", "", "get order by id")

	orderCmd.Flags().BoolVar(&OrderFlags.IsLocal, "local", false, "get info from local db or not")
	orderCmd.Flags().BoolVar(&OrderFlags.Cancel, "cancel", false, "cancel order")

	orderCmd.Flags().StringVar(&OrderFlags.Status, "status", "", "cancel order by status. valid statuse are open|new")
	orderCmd.Flags().BoolVar(&OrderFlags.Csv, "csv", false, "the orders canceled shall be printed on stdout in csv format if true")

	orderCmd.Flags().SortFlags = false

	return orderCmd
}

func executeOrderCommand(b bot.Sp1g0tBot) utils.RunEFunc {
	return func(cmd *cobra.Command, args []string) error {
		if OrderFlags.Place {
			if OrderFlags.Pair == "" || OrderFlags.Price == "" || OrderFlags.Side == "" || OrderFlags.Size == "" || OrderFlags.Type == "" {
				return fmt.Errorf("one of the required flag is missed. Please check pair, price, side, size, type flags are presented and are not empty")
			}
			pairData, err := b.GetMarketPairData(OrderFlags.Pair)
			if err != nil {
				return err
			}

			order, err := b.PlaceOrder(utils.TrimSpacesFromEnv(os.Getenv(utils.WALLET)), pairData.Market.Name, pairData.Market.MarketAddress, OrderFlags.Price,
				OrderFlags.Side, OrderFlags.Size, OrderFlags.Type)
			if err != nil {
				return err
			}
			logrus.Infof("order placed. %+v", order.Orders)

			return nil
		}

		if OrderFlags.OrderID != "" && !OrderFlags.Cancel {
			order, err := b.GetOrder(OrderFlags.OrderID, OrderFlags.IsLocal)
			if err != nil {
				return err
			}
			logrus.Infof("order: %+v", order.Orders)
			return nil
		}

		if OrderFlags.Cancel {
			if OrderFlags.OrderID != "" {
				logrus.Info("canceling order")
				canceledOrder, err := b.CancelOrder(OrderFlags.OrderID)
				if err != nil {
					return err
				}
				logrus.Infof("canceled order: %+v", canceledOrder.Orders)
				return nil
			}
			if OrderFlags.Status != "" {
				err := utils.CheckOrderStatus(OrderFlags.Status)
				if err != nil {
					return err
				}

				err = b.CancelOrdersByStatus(OrderFlags.Status)
				if err != nil {
					return err
				}
				return nil
			}
			if OrderFlags.Csv {
				err := b.PrintCanceledOrders()
				if err != nil {
					return err
				}
				return nil
			}
		}

		return cmd.Help()
	}
}
