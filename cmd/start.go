package cmd

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/Evrynet/sp1g0t/bot"
	"gitlab.com/Evrynet/sp1g0t/config"
	"gitlab.com/Evrynet/sp1g0t/utils"
)

func newStartCommand(b bot.Sp1g0tBot) *cobra.Command {
	// startCmd represents the start command
	startCmd := &cobra.Command{
		Use:   "start",
		Short: "Starts trading using saved configs",
		Long:  `Starts trading using saved configs`,
		RunE:  executeStartCommand(b),
	}
	return startCmd
}

// initConfigs read data from .env and build cfg object for bot execution
func initConfigs(b bot.Sp1g0tBot) (*config.Sp1g0tConfig, error) {
	logrus.Debug("Getting market pairs..")
	markets, err := b.GetMarketPairs("", "")
	if err != nil {
		return nil, err
	}
	logrus.Debug("Market pair are loaded")

	logrus.Debug("Init from env")
	cfg, err := config.InitConfigs(markets)
	if err != nil {
		logrus.Errorf("error with initting from env variables.")
		return nil, err
	}
	logrus.Debug("Initted from env with success")
	return cfg, nil
}

func executeStartCommand(b bot.Sp1g0tBot) utils.RunEFunc {
	return func(cmd *cobra.Command, args []string) error {
		logrus.Info("Getting configurations ... ")
		botConfig, err := initConfigs(b)
		if err != nil {
			logrus.Errorf("Cannot init from env. Error = %s. Please check your env variables", err)
			return err
		}
		b.BotConfig = botConfig
		logrus.Info("DONE")
		logrus.Info("Starting bot ... ")

		b.Start()
		return nil
	}
}
