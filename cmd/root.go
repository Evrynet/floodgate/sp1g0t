package cmd

import (
	"fmt"
	"os"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/Evrynet/sp1g0t/bot"
	"gitlab.com/Evrynet/sp1g0t/config"
	"gitlab.com/Evrynet/sp1g0t/models"
	"gitlab.com/Evrynet/sp1g0t/utils"
)

var botConfig config.Sp1g0tConfig

var rootCmd = &cobra.Command{
	Use:   "sp1g0t",
	Short: fmt.Sprintf("USAGE %s [OPTIONS]", os.Args[0]),
	Long:  fmt.Sprintf(`USAGE %s [OPTIONS] : see --help for details`, os.Args[0]),
}

func registerCmds(b bot.Sp1g0tBot) {
	rootCmd.AddCommand(newStartCommand(b))
	rootCmd.AddCommand(newOrderCommand(b))
}

// Execute adds all child commands to the root
func Execute(b bot.Sp1g0tBot) error {
	rootCmd.RunE = execute(b)
	registerCmds(b)
	rootCmd.CompletionOptions.DisableDefaultCmd = true

	rootCmd.Flags().BoolVar(&GlobalFlags.TokensList, "list-tokens", false, "Show list of tokens")
	rootCmd.Flags().BoolVar(&GlobalFlags.MarketsList, "list-markets", false, "Show list of markets")
	rootCmd.Flags().StringVar(&GlobalFlags.Basep, "basep", "", "base asset")
	rootCmd.Flags().StringVar(&GlobalFlags.Quotep, "quotep", "", "quote asset")
	rootCmd.Flags().Lookup("basep").Hidden = true
	rootCmd.Flags().Lookup("quotep").Hidden = true
	rootCmd.Flags().Lookup("list-markets").Value.Set("basep")
	rootCmd.Flags().Lookup("list-markets").Value.Set("quotep")

	rootCmd.Flags().StringVar(&GlobalFlags.ToCSV, "csv", "", "Download all markets as csv file. If name is empty - saves as 'markets.csv' to current working directory.")
	rootCmd.Flags().Lookup("csv").NoOptDefVal = "markets.csv"

	rootCmd.Flags().BoolVar(&GlobalFlags.Register, "register", false, "Register in API")

	rootCmd.Flags().BoolVar(&GlobalFlags.Verify, "verify", false, "Verifies your user with token from email in API. NOTE: add flag '--code={token}'")
	rootCmd.Flags().StringVar(&GlobalFlags.Code, "code", "", "Verifies your user with token from email in API")
	rootCmd.Flags().Lookup("code").Hidden = true
	rootCmd.Flags().Lookup("verify").Value.Set("code")

	return rootCmd.Execute()
}

func execute(b bot.Sp1g0tBot) utils.RunEFunc {
	return func(cmd *cobra.Command, args []string) error {
		if GlobalFlags.Register {
			logrus.Info("Registering in API...")
			err := b.Register(models.RegistrationReq{
				OrgName:       utils.TrimSpacesFromEnv(os.Getenv(utils.ORGANIZATION_NAME)),
				Email:         utils.TrimSpacesFromEnv(os.Getenv(utils.EMAIL)),
				WalletAddress: utils.TrimSpacesFromEnv(os.Getenv(utils.WALLET)),
			})
			if err != nil {
				return err
			}
			logrus.Info("Success! Please check your email and run '--verify' with provided token for validation.")
			return nil
		}
		if GlobalFlags.Code != "" {
			err := b.Verify(GlobalFlags.Code)
			if err != nil {
				return err
			}
			logrus.Info("Successfully verified. Now you can run 'start' command to run the bot. Happy trading :)")
			return nil
		}
		if GlobalFlags.TokensList {
			tokensList, err := b.GetTokensList()
			if err != nil {
				logrus.Error("Error while fetching token list. ", err)
				return err
			}
			logrus.Info("Available tokens: ", tokensList)
			return nil
		}
		if GlobalFlags.MarketsList {
			var basep, quotep string
			switch {
			case GlobalFlags.Basep != "" && GlobalFlags.Quotep == "":
				basep = GlobalFlags.Basep
			case GlobalFlags.Basep == "" && GlobalFlags.Quotep != "":
				quotep = GlobalFlags.Quotep
			default:
				quotep = GlobalFlags.Quotep
				basep = GlobalFlags.Basep
			}

			markets, err := b.GetMarkets(basep, quotep)
			if err != nil {
				logrus.Error("Error while fetching markets. ", err)
				return err
			}
			logrus.Infof("Available markets: %+v", markets)
			return nil

		}
		if GlobalFlags.ToCSV != "" {
			err := b.MarketsToCSV(GlobalFlags.ToCSV)
			if err != nil {
				logrus.Error("Error while building markets csv. ", err)
				return err
			}
			return nil
		}

		return cmd.Help()
	}
}
