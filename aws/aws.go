package aws

type Aws struct {
	Ses  SES
	Smtp SMTP
}

func New(ses SES, smtp SMTP) Aws {
	return Aws{ses, smtp}
}
