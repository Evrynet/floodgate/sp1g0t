package aws

import (
	"crypto/tls"
	"log"
	"net/smtp"
	"net/textproto"
	"os"
	"strconv"
	"strings"

	"github.com/jordan-wright/email"
)

type SMTP struct {
	host         string
	port         string
	userName     string
	password     string
	from         string
	smtpIsSecure bool
	templatesDir string
	serverAddr   string
}

func NewSMTP() SMTP {
	smtpIsSecure, err := strconv.ParseBool(os.Getenv("SMTP_IS_SECURE"))
	if err != nil {
		log.Panic(err)
	}

	host := os.Getenv("SMTP_HOST")
	port := os.Getenv("SMTP_PORT")
	serverAddr := host + ":" + port

	return SMTP{
		host:         host,
		port:         port,
		userName:     os.Getenv("SMTP_AUTH_USERNAME"),
		password:     os.Getenv("SMTP_AUTH_PASSWORD"),
		from:         strings.Replace(os.Getenv("SMTP_DEFAULT_SENDER"), "\\\"", "", -1),
		smtpIsSecure: smtpIsSecure,
		templatesDir: os.Getenv("SMTP_TEMPLATES_DIR"),
		serverAddr:   serverAddr,
	}
}

func (s SMTP) Send(recipient, subject, message string) error {
	e := &email.Email{
		To:      []string{recipient},
		From:    s.from,
		Subject: subject,
		Text:    []byte(message),
		//HTML:    []byte("<h1>Fancy HTML is supported, too!</h1>"),
		Headers: textproto.MIMEHeader{},
	}

	auth := smtp.PlainAuth("", s.userName, s.password, s.host)

	// TLS config
	tlsconfig := &tls.Config{
		InsecureSkipVerify: s.smtpIsSecure,
		ServerName:         s.host,
	}

	err := e.SendWithTLS(s.serverAddr, auth, tlsconfig)
	if err != nil {
		return err
	}
	return nil
}
