package logger

import (
	"github.com/sirupsen/logrus"
	prefixed "github.com/x-cray/logrus-prefixed-formatter"
)

func init() {
	formatter := &prefixed.TextFormatter{
		ForceFormatting:  true,
		DisableTimestamp: false,
		FullTimestamp:    true,
		QuoteEmptyFields: true,
	}

	logrus.SetFormatter(formatter)
}
