package utils

import (
	"errors"
	"io/ioutil"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func Test_OkayToSendWarning(t *testing.T) {
	os.Setenv("EMAIL_HOLD_OFF", "1800")
	lastTS := time.Now().UTC().Add(-1801 * time.Second)
	jts, err := lastTS.MarshalJSON()
	assert.Nil(t, err)
	err = ioutil.WriteFile(warningPath, jts, 0600)
	assert.Nil(t, err)
	res := OkayToSendWarning()
	assert.True(t, res)
}

func Test_OkayToSendWarning_TooEarly(t *testing.T) {
	os.Setenv("EMAIL_HOLD_OFF", "1800")
	lastTS := time.Now().UTC().Add(-1 * time.Second)
	jts, err := lastTS.MarshalJSON()
	assert.Nil(t, err)
	err = ioutil.WriteFile(warningPath, jts, 0600)
	assert.Nil(t, err)
	res := OkayToSendWarning()
	assert.False(t, res)
}

func Test_OkayToSendWarning_Garbage(t *testing.T) {
	os.Setenv("EMAIL_HOLD_OFF", "1800")
	err := ioutil.WriteFile(warningPath, []byte("garbage"), 0600)
	assert.Nil(t, err)
	res := OkayToSendWarning()
	assert.False(t, res)
	_, err = os.Open(warningPath)
	assert.True(t, errors.Is(err, os.ErrNotExist))
}

func Test_OkayToSendWarning_Garbage_Env(t *testing.T) {
	os.Setenv("EMAIL_HOLD_OFF", "garbage")
	lastTS := time.Now().UTC().Add(-1 * time.Second)
	jts, err := lastTS.MarshalJSON()
	assert.Nil(t, err)
	err = ioutil.WriteFile(warningPath, jts, 0600)
	assert.Nil(t, err)
	res := OkayToSendWarning()
	assert.False(t, res)
}

func Test_OkayToSendWarning_Garbage_Env_Okay(t *testing.T) {
	os.Setenv("EMAIL_HOLD_OFF", "garbage")
	lastTS := time.Now().UTC().Add(-3601 * time.Second)
	jts, err := lastTS.MarshalJSON()
	assert.Nil(t, err)
	err = ioutil.WriteFile(warningPath, jts, 0600)
	assert.Nil(t, err)
	res := OkayToSendWarning()
	assert.True(t, res)
}

func Test_OkayToSendWarning_Repeated(t *testing.T) {
	os.Setenv("EMAIL_HOLD_OFF", "1800")
	lastTS := time.Now().UTC().Add(-1801 * time.Second)
	jts, err := lastTS.MarshalJSON()
	assert.Nil(t, err)
	err = ioutil.WriteFile(warningPath, jts, 0600)
	assert.Nil(t, err)
	res := OkayToSendWarning()
	assert.True(t, res)
	res = OkayToSendWarning()
	assert.False(t, res)
}

func Test_OkayToSendWarning_Repeated_And_Delayed(t *testing.T) {
	os.Setenv("EMAIL_HOLD_OFF", "1")
	lastTS := time.Now().UTC().Add(-10 * time.Second)
	jts, err := lastTS.MarshalJSON()
	assert.Nil(t, err)
	err = ioutil.WriteFile(warningPath, jts, 0600)
	assert.Nil(t, err)
	res := OkayToSendWarning()
	assert.True(t, res)
	time.Sleep(2 * time.Second)
	res = OkayToSendWarning()
	assert.True(t, res)
}

func Test_OkayToSendWarning_Zero_HoldOff(t *testing.T) {
	os.Setenv("EMAIL_HOLD_OFF", "")
	lastTS := time.Now().UTC().Add(-1 * time.Second)
	jts, err := lastTS.MarshalJSON()
	assert.Nil(t, err)
	err = ioutil.WriteFile(warningPath, jts, 0600)
	assert.Nil(t, err)
	res := OkayToSendWarning()
	assert.False(t, res)
}
