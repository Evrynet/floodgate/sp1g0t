package utils

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net"
	"net/mail"
	"net/smtp"
	"os"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
)

const (
	forceDisconnectAfter = time.Second * 5
	hostPort             = 465
	warningPath          = "/tmp/last-warning-timestamp.sp1g0t"
)

var (
	emailRegexp = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
	hol         sync.Mutex
)

func OkayToSendWarning() bool {
	hol.Lock()
	defer hol.Unlock()
	logrus.Info("> OkayToSendWarning")

	var now, lastWarning time.Time
	now = time.Now().UTC()
	jts, err := ioutil.ReadFile(warningPath)
	if err != nil {
		// file does not exist?
		logrus.Errorf("failed to read last warning timestamp. err = %s", err)
		err = writeTS(now, warningPath)
		if err != nil {
			return false
		}
		lastWarning = now
	} else {
		err = lastWarning.UnmarshalJSON(jts)
		if err != nil {
			logrus.Errorf("failed to unmarshal last warning timestamp. err = %s", err)
			// corrupted timestamp? remove the file.
			os.Remove(warningPath)
			return false
		}
	}
	logrus.Infof("> lastWarning: %v", lastWarning)
	// convert to seconds
	elapsed := int64(time.Since(lastWarning) / 1e9)
	logrus.Infof("> elapsed: %v", elapsed)
	hoev := os.Getenv("EMAIL_HOLD_OFF")
	var holdOff int64
	if hoev != "" {
		holdOff, err = strconv.ParseInt(hoev, 10, 32)
		if err != nil {
			logrus.Errorf("invalid EMAIL_HOLD_OFF value? err = %s", err)
		}
	}
	if holdOff == 0 {
		// initalize to sane value
		holdOff = 3600
	}
	logrus.Infof("> holdOff: %v", holdOff)

	if elapsed > holdOff {
		err = writeTS(now, warningPath)
		if err != nil {
			return false
		}
		return true
	}
	return false
}

func writeTS(ts time.Time, path string) error {
	jts, err := ts.MarshalJSON()
	if err != nil {
		logrus.Errorf("failed to marshal timestamp. err = %s", err)
		return err
	}
	err = ioutil.WriteFile(path, jts, 0600)
	if err != nil {
		logrus.Errorf("failed to write timestamp. err = %s", err)
		return err
	}
	return nil
}

//TODO: probably we need SMTP server to check reverse validation
//https://github.com/badoux/checkmail#3-host-and-user
func ValidateEmail(email string) error {
	err := validateFormat(email)
	if err != nil {
		return err
	}

	//NOTE: temporary turned off
	// hostEmail, err := prettifyEmail(os.Getenv("SMTP_DEFAULT_SENDER"))
	// if err != nil {
	// 	return err
	// }

	// err = validateHostAndUser(os.Getenv("SMTP_HOST"), hostEmail, email)
	// if err != nil {
	// 	return err
	// }

	return nil
}

// ValidateHostAndUser validate mail host and user.
// If host is valid, requires valid SMTP [1] serverHostName and serverMailAddress to reverse validation
// for prevent SPAN and BOTS.
// [1] https://mxtoolbox.com/SuperTool.aspx
func validateHostAndUser(serverHostName, serverMailAddress, email string) error {
	_, host := split(email)
	mx, err := net.LookupMX(host)
	if err != nil {
		return errors.New("unresolvable host")
	}
	client, err := dialTimeout(fmt.Sprintf("%s:%d", mx[0].Host, hostPort), forceDisconnectAfter)
	if err != nil {
		return err
	}
	defer client.Close()

	err = client.Hello(serverHostName)
	if err != nil {
		return err
	}
	err = client.Mail(serverMailAddress)
	if err != nil {
		return err
	}
	err = client.Rcpt(email)
	if err != nil {
		return err
	}
	return nil
}

// dialTimeout returns a new Client connected to an SMTP server at addr.
// The addr must include a port, as in "mail.example.com:smtp".
func dialTimeout(addr string, timeout time.Duration) (*smtp.Client, error) {
	conn, err := net.DialTimeout("tcp", addr, timeout)
	if err != nil {
		return nil, err
	}

	t := time.AfterFunc(timeout, func() { conn.Close() })
	defer t.Stop()

	host, _, _ := net.SplitHostPort(addr)
	return smtp.NewClient(conn, host)
}

func split(email string) (account, host string) {
	i := strings.LastIndexByte(email, '@')
	account = email[:i]
	host = email[i+1:]
	return
}

func validateFormat(email string) error {
	if !emailRegexp.MatchString(email) {
		return errors.New("email format is invalid")
	}
	return nil
}

func prettifyEmail(email string) (string, error) {
	res, err := mail.ParseAddress(email)
	if err != nil {
		return "", err
	}
	return res.Address, nil
}
