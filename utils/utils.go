package utils

import (
	"fmt"
	"strings"
	"time"

	"github.com/shopspring/decimal"
	"github.com/spf13/cobra"
)

// RunEFunc runs a cobra function returning an error.
type RunEFunc func(cmd *cobra.Command, args []string) error

func CheckOrderStatus(status string) error {
	if strings.ToUpper(status) == ORDER_STATUS_NEW || strings.ToUpper(status) == ORDER_STATUS_OPEN {
		return nil
	}
	return fmt.Errorf("it is impossible to cancel order with status %s", strings.ToUpper(status))
}

//ParseStringToTime parses string represantation of time to time object
func ParseStringToTime(timeStr string) (time.Time, error) {
	t, err := time.Parse(NOT_TRADE_PERIOD_LAYOUT, timeStr)
	if err != nil {
		return time.Time{}, err
	}
	return t, nil
}

//((marketPrice-lowestPrice)/marketPrice) * 100
func PercentageDeviation(marketPrice, lowestPrice decimal.Decimal) decimal.Decimal {
	return marketPrice.
		Sub(lowestPrice).
		Div(marketPrice).
		Mul(decimal.NewFromInt(100)).Abs()
}

func FindTheClosestBiggestWithoutReminder(first, second decimal.Decimal) decimal.Decimal {
	return first.Add(second).Sub(first.Add(second).Mod(second))
}

func TrimSpacesFromEnv(envVar string) string {
	return strings.TrimSpace(envVar)
}
