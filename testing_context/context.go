package testing_context

import (
	"context"

	"gorm.io/gorm"
)

type contextKey string

func (c contextKey) String() string {
	return string(c)
}

var (
	ContextKeyDB = contextKey("db")
)

func GetDbFromContext(ctx context.Context) *gorm.DB {
	return ctx.Value(ContextKeyDB).(*gorm.DB)
}
