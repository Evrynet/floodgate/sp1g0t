package bot

import (
	"context"
	"testing"
	"time"

	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/shopspring/decimal"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"gitlab.com/Evrynet/sp1g0t/aws"
	"gitlab.com/Evrynet/sp1g0t/config"
	"gitlab.com/Evrynet/sp1g0t/db"
	"gitlab.com/Evrynet/sp1g0t/db/repository"
	"gitlab.com/Evrynet/sp1g0t/hydra"
	"gitlab.com/Evrynet/sp1g0t/models"
	"gitlab.com/Evrynet/sp1g0t/providers"
	"gitlab.com/Evrynet/sp1g0t/providers/coingecko"
	"gitlab.com/Evrynet/sp1g0t/providers/gateio"
	"gitlab.com/Evrynet/sp1g0t/testing_context"
	"gitlab.com/Evrynet/sp1g0t/utils"
	"gorm.io/gorm"
)

type TraidingTestSuite struct {
	suite.Suite
	db  *gorm.DB
	tx  *gorm.DB
	ctx context.Context

	bot      Sp1g0tBot
	repo     repository.Repository
	provider *providers.PriceProvider
	mock     hydra.HydraMock
}

func (suite *TraidingTestSuite) SetupTest() {
	gormDB, err := db.New(db.GetDBAuthFromEnv())
	if err != nil {
		logrus.Fatal(err)
	}

	suite.db = gormDB
	suite.mock = hydra.HydraMock{}

	coingeckoMock := coingecko.NewCoingecko(&coingecko.CoingeckoApiMock{}, true)
	gateIOMock := gateio.NewGateIO(gateio.GateeIOApiMock{})
	suite.provider = providers.NewPriceProvider(coingeckoMock, gateIOMock)
}

func (suite *TraidingTestSuite) BeforeTest(suiteName, testName string) {
	suite.tx = suite.db.Begin()
	suite.repo = repository.NewRepository(repository.NewOrderRepository(suite.tx), repository.NewSettlesRepository(suite.tx),
		repository.NewPricesRepository(suite.tx), repository.NewWalletBalancesRepository(suite.tx))
	suite.bot = NewSp1g0tBot(&suite.mock, nil, suite.repo, suite.provider, aws.Aws{})
	suite.ctx = context.WithValue(context.Background(), testing_context.ContextKeyDB, suite.tx)
}

func (suite *TraidingTestSuite) AfterTest(suiteName, testName string) {
	suite.tx.Rollback()
}

func (suite *TraidingTestSuite) Test_cancelOwnOrders() {
	ordersData := []models.OrderData{
		{
			Id:            "24e29215-445c-470e-a20c-85d296b7a991",
			ClientId:      "client1",
			MarketAddress: "address1",
			Price:         "41000",
			Side:          "sell",
			Size:          "2",
			Market:        "BTC/USDT",
			Status:        utils.ORDER_STATUS_OPEN,
		},
		{
			Id:            "24e29215-445c-470e-a20c-85d296b7a992",
			ClientId:      "client1",
			MarketAddress: "address1",
			Price:         "50000",
			Side:          "sell",
			Size:          "1",
			Market:        "BTC/USDT",
			Status:        utils.ORDER_STATUS_OPEN,
		},
		{
			Id:            "24e29215-445c-470e-a20c-85d296b7a993",
			ClientId:      "client1",
			MarketAddress: "address1",
			Price:         "39000",
			Side:          "buy",
			Size:          "2",
			Market:        "BTC/USDT",
			Status:        utils.ORDER_STATUS_OPEN,
		},
		{
			Id:            "24e29215-445c-470e-a20c-85d296b7a994",
			ClientId:      "client1",
			MarketAddress: "address1",
			Price:         "40500",
			Side:          "buy",
			Size:          "1",
			Market:        "BTC/USDT",
			Status:        utils.ORDER_STATUS_OPEN,
		},
		{
			Id:            "24e29215-445c-470e-a20c-85d296b7a995",
			ClientId:      "client1",
			MarketAddress: "address1",
			Price:         "35000",
			Side:          "buy",
			Size:          "1",
			Market:        "BTC/USDT",
			Status:        utils.ORDER_STATUS_OPEN,
		},
		{
			Id:            "24e29215-445c-470e-a20c-85d296b7a996",
			ClientId:      "client1",
			MarketAddress: "address2",
			Price:         "35000",
			Side:          "buy",
			Size:          "1",
			Market:        "BTC/USDT",
			Status:        utils.ORDER_STATUS_OPEN,
		},
	}

	for _, od := range ordersData {
		repository.GivenAnOrder().OrderData(od).Build(suite.ctx)
	}

	suite.bot.BotConfig = &config.Sp1g0tConfig{
		SafetyMargin: decimal.NewFromInt(5),
		AssetPairCfgMap: map[string]*config.AssetPairCfg{
			"BTC_USDT": {
				BidMargin: decimal.NewFromInt(4),
				AskMargin: decimal.NewFromInt(8),
			},
		},
	}

	err := suite.bot.cancelOwnOutOfRangeOrders("address1", decimal.NewFromInt(40000))
	suite.Nil(err)

	ordersAfterCanceling, err := suite.bot.Repository.Order.GetOrdersByStatus(utils.ORDER_STATUS_CANCELED)
	suite.Nil(err)

	suite.Equal(3, len(ordersAfterCanceling))
}

func (suite *TraidingTestSuite) Test_checkPortfolio() {
	markets := []models.MarketData{
		{
			BaseMintAddr:  "solana_addr",
			Base:          "SOL",
			Quote:         "USDT",
			MarketAddress: "solana_address",
		},
		{
			MarketAddress: "bitcoin_address",
			BaseMintAddr:  "some_mint_addr2",
			Base:          "BTC",
			Quote:         "USDT",
		},
	}
	balances, err := suite.bot.GetWalletBalances()
	suite.Nil(err)

	suite.bot.BotConfig = &config.Sp1g0tConfig{
		LossPeriod:    600, //10mins
		LossThreshold: decimal.NewFromInt(100),
	}
	now := time.Date(2022, 1, 1, 10, 0, 0, 0, time.UTC)

	isPortfoliOk := suite.bot.checkPortfolio(now, markets, balances.Balances)
	suite.True(isPortfoliOk)

	now = time.Date(2022, 1, 1, 10, 10, 0, 0, time.UTC)
	balances.Balances[0].Total = "10"
	balances.Balances[1].Total = "20"

	isPortfoliOk = suite.bot.checkPortfolio(now, markets, balances.Balances)
	suite.False(isPortfoliOk)
}

func (suite *TraidingTestSuite) Test_filterMarketsForTrading() {
	balances := []models.BalanceData{
		{
			Asset:    "BTC",
			MintAddr: "btc_base_mint_address",
			Total:    "100",
		},
		{
			Asset:    "Solana",
			MintAddr: "sol_base_mint_address",
			Total:    "99",
		},
	}

	tradeOnlyConstraint := make(map[string][]string)
	tradeOnlyConstraint["BTC"] = []string{"BTC", "USDT"}
	suite.bot.BotConfig = &config.Sp1g0tConfig{
		MarketsTradeOnly: tradeOnlyConstraint,
	}

	marketsForTrading := suite.bot.filterMarketsForTrading(balances)
	suite.Len(marketsForTrading, 1)

	tradeOnlyConstraint["SOL"] = []string{"SOL", "USDT"}
	marketsForTrading = suite.bot.filterMarketsForTrading(balances)
	suite.Len(marketsForTrading, 2)
}
func TestTraidingTestSuite(t *testing.T) {
	suite.Run(t, new(TraidingTestSuite))
}
