package bot

import (
	"encoding/csv"
	"os"
	"sort"
	"strings"

	"github.com/dnlo/struct2csv"
	"github.com/shopspring/decimal"
	"github.com/sirupsen/logrus"
	"gitlab.com/Evrynet/sp1g0t/aws"
	"gitlab.com/Evrynet/sp1g0t/config"
	"gitlab.com/Evrynet/sp1g0t/db"
	"gitlab.com/Evrynet/sp1g0t/db/repository"
	"gitlab.com/Evrynet/sp1g0t/hydra"
	"gitlab.com/Evrynet/sp1g0t/models"
	"gitlab.com/Evrynet/sp1g0t/providers"
	"gitlab.com/Evrynet/sp1g0t/utils"
)

type Sp1g0tBot struct {
	hydraAPI      hydra.HydraInterface
	Migrator      *db.Migration
	Repository    repository.Repository
	BotConfig     *config.Sp1g0tConfig
	PriceProvider *providers.PriceProvider
	AwsService    aws.Aws
}

func NewSp1g0tBot(hydraAPI hydra.HydraInterface, migrator *db.Migration, repository repository.Repository,
	priceProvider *providers.PriceProvider, awsService aws.Aws) Sp1g0tBot {
	return Sp1g0tBot{
		hydraAPI:      hydraAPI,
		Migrator:      migrator,
		Repository:    repository,
		PriceProvider: priceProvider,
		AwsService:    awsService,
	}
}

//GetTokensList list all tokens (from the set of asset pairs supported by the API server) in alphabetical order
func (b Sp1g0tBot) GetTokensList() (tokenList []string, err error) {
	pairs, err := b.GetMarketPairs("", "")
	if err != nil {
		return
	}
	for _, pair := range pairs {
		if !contains(tokenList, pair) {
			tokenList = append(tokenList, pair)
		}
	}

	sort.Strings(tokenList)

	return
}

func (b Sp1g0tBot) MarketsToCSV(fileName string) (err error) {
	markets, err := b.GetMarkets("", "")
	if err != nil {
		return
	}

	enc := struct2csv.New()
	rows, err := enc.Marshal(markets)
	if err != nil {
		logrus.Error("failed to marshal fetched markets")
		return
	}

	csvFile, err := os.Create(fileName)
	if err != nil {
		logrus.Errorf("failed to create file with name %s", fileName)
		return
	}

	defer func() {
		if err := csvFile.Close(); err != nil {
			logrus.Errorf("Error closing file: %s\n", err)
		}
	}()

	csvwriter := csv.NewWriter(csvFile)

	err = csvwriter.WriteAll(rows)
	if err != nil {
		logrus.Error("failed to write data into file")
		return
	}
	return
}

func (b Sp1g0tBot) PrintCanceledOrders() error {
	orders, err := b.GetOrdersByStatus(utils.ORDER_STATUS_CANCELED)
	if err != nil {
		return err
	}

	enc := struct2csv.New()
	rows, err := enc.Marshal(orders)
	if err == struct2csv.ErrNilSlice || err == struct2csv.ErrEmptySlice {
		logrus.Warn("There are no canceled orders.")
		return nil
	} else if err != nil {
		return err
	}

	writer := csv.NewWriter(os.Stdout)
	for _, row := range rows {
		err := writer.Write(row)
		if err != nil {
			return err
		}
	}
	if writer.Error() != nil {
		return err
	}
	writer.Flush()

	return nil
}

func (b Sp1g0tBot) GetMarkets(basep, quotep string) ([]models.MarketData, error) {
	marketPairs, err := b.GetMarketPairs(basep, quotep)
	if err != nil {
		return nil, err
	}

	var markets []models.MarketData
	for _, pair := range marketPairs {
		marketData, err := b.GetMarketPairData(pair)
		if err != nil {
			continue
		}
		markets = append(markets, marketData.Market)
	}
	return markets, nil
}

func (b Sp1g0tBot) CancelOrdersByStatus(status string) error {
	orders, err := b.GetOrdersByStatus(strings.ToUpper(status))
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"method": "CancelOrdersByStatus",
			"call":   "GetOrdersByStatus",
			"status": status,
		}).Errorf("failed to cancel order by status. err = %s", err)
		return err
	}
	for _, order := range orders {
		_, err := b.CancelOrder(order.Id)
		if err != nil {
			return err
		}
	}
	return nil
}

func (b Sp1g0tBot) calculateWalletValue(balances []models.BalanceData, markets []models.MarketData) decimal.Decimal {
	var totalInUSD decimal.Decimal
	for _, balance := range balances {
		for _, market := range markets {
			if market.BaseMintAddr == balance.MintAddr {
				avgUSDPrice, err := b.GetPriceForAssetPair(market.Name, market.MarketAddress)
				if err != nil {
					return decimal.Decimal{}
				}
				quantityDecimal, err := decimal.NewFromString(balance.Total)
				if err != nil {
					logrus.Errorf("cannot calculate quantityDecimal for %s", market.Base)
					return decimal.Decimal{}
				}
				totalInUSD = totalInUSD.Add(avgUSDPrice.Mul(quantityDecimal))
				break
			}
		}
	}
	return totalInUSD
}

//cancelAllOwnOrders cancel all own orders for given market
func (b Sp1g0tBot) cancelAllOwnOrders(marketAddress string) error {
	ownOrders, err := b.GetOwnOrdersForMarket(marketAddress)
	if err != nil {
		logrus.Error(err)
		return err
	}
	for _, order := range ownOrders {
		_, err := b.CancelOrder(order.Id)
		if err != nil {
			continue
		}
	}
	return nil
}

//cancelOwnOpenOrders cancel all own orders with Open status from all markets
func (b Sp1g0tBot) cancelOwnOpenOrders() error {
	ownOrders, err := b.GetOrdersByStatus(utils.ORDER_STATUS_OPEN)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"method": "cancelOwnOpenOrders",
			"call":   "GetOrdersByStatus",
		}).Errorf("failed to get order by OPEN status: %s", err.Error())
		return err
	}
	for _, order := range ownOrders {
		_, err := b.CancelOrder(order.Id)
		if err != nil {
			continue
		}
	}
	return nil
}

func (b Sp1g0tBot) saveMarketPrice(asset string, marketPrice decimal.Decimal) error {
	err := b.SaveAssetPrice(asset, marketPrice)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"method":      "saveMarketPrice",
			"call":        "SaveAssetPrice",
			"asset":       asset,
			"marketPrice": marketPrice,
		}).Errorf("error while saving price to db: %s", err.Error())
		return err
	}
	return nil
}

func (b Sp1g0tBot) filterOrderbookInRange(marketAddress, market string, marketPrice decimal.Decimal) ([]models.OrderbookData, []models.OrderbookData, error) {
	var asksFiltered, bidsFiltered []models.OrderbookData
	orderbook, err := b.GetOrderBook(marketAddress)
	if err != nil {
		return nil, nil, err
	}

	for _, order := range orderbook.Asks {
		isInRange, err := b.isFOinRange(marketPrice, order.Price, market, utils.SIDE_SELL)
		if err != nil {
			return nil, nil, err
		}
		if isInRange {
			asksFiltered = append(asksFiltered, order)
		}
	}

	for _, order := range orderbook.Bids {
		isInRange, err := b.isFOinRange(marketPrice, order.Price, market, utils.SIDE_BUY)
		if err != nil {
			return nil, nil, err
		}
		if isInRange {
			bidsFiltered = append(bidsFiltered, order)
		}
	}

	return asksFiltered, bidsFiltered, nil
}

func calculateOrderbookDepth(asks []models.OrderbookData, bids []models.OrderbookData) (askDepth decimal.Decimal, bidDepth decimal.Decimal, err error) {
	for _, v := range asks {
		depth, err := getOrderDepth(v.Size, v.Price)
		if err != nil {
			return decimal.Decimal{}, decimal.Decimal{}, err
		}
		askDepth = askDepth.Add(depth)
	}

	for _, v := range bids {
		depth, err := getOrderDepth(v.Size, v.Price)
		if err != nil {
			return decimal.Decimal{}, decimal.Decimal{}, err
		}
		bidDepth = bidDepth.Add(depth)
	}
	return
}

func (b Sp1g0tBot) filterOwnOrdersInRange(marketAddress string, marketPrice decimal.Decimal) ([]repository.Order, error) {
	var filteredOrders []repository.Order
	localOrders, err := b.GetOwnOrdersForMarket(marketAddress)
	if err != nil {
		return nil, err
	}

	for _, order := range localOrders {
		isInRange, err := b.isFOinRange(marketPrice, order.Price, order.Market, order.Side)
		if err != nil {
			return nil, err
		}
		if isInRange {
			filteredOrders = append(filteredOrders, order)
		}
	}

	return filteredOrders, err
}

func calculateOwnOrdersDepth(filteredOrders []repository.Order) (askDepth decimal.Decimal, bidDepth decimal.Decimal, err error) {
	for _, order := range filteredOrders {
		if order.Side == utils.SIDE_SELL {
			depth, err := getOrderDepth(order.Size, order.Price)
			if err != nil {
				return decimal.Decimal{}, decimal.Decimal{}, err
			}
			askDepth = askDepth.Add(depth)
		}
		if order.Side == utils.SIDE_BUY {
			depth, err := getOrderDepth(order.Size, order.Price)
			if err != nil {
				return decimal.Decimal{}, decimal.Decimal{}, err
			}
			bidDepth = bidDepth.Add(depth)
		}
	}
	return
}
