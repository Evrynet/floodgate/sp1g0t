package bot

import (
	"time"

	"github.com/google/uuid"
	"gitlab.com/Evrynet/sp1g0t/db/repository"
	"gitlab.com/Evrynet/sp1g0t/models"
)

func orderToRepoOrder(signedOrder models.OrderData, txType, txID string) repository.Order {
	return repository.Order{
		OrderData: signedOrder,
		TxType:    txType,
		TxID:      txID,
	}
}

func repoWalletBalanceToBalanceData(wb repository.WalletBalance) models.BalanceData {
	return models.BalanceData{
		Asset:    wb.Asset,
		MintAddr: wb.MintAddr,
		Total:    wb.Total,
	}
}

func balanceDataToRepo(bd models.BalanceData, createdAt time.Time) *repository.WalletBalance {
	return &repository.WalletBalance{
		ID:        uuid.New(),
		Asset:     bd.Asset,
		MintAddr:  bd.MintAddr,
		Total:     bd.Total,
		CreatedAt: createdAt,
	}
}
