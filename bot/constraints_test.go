package bot

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/shopspring/decimal"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/suite"
	"gitlab.com/Evrynet/sp1g0t/aws"
	"gitlab.com/Evrynet/sp1g0t/config"
	"gitlab.com/Evrynet/sp1g0t/db"
	"gitlab.com/Evrynet/sp1g0t/db/repository"
	"gitlab.com/Evrynet/sp1g0t/hydra"
	"gitlab.com/Evrynet/sp1g0t/models"
	"gitlab.com/Evrynet/sp1g0t/providers"
	"gitlab.com/Evrynet/sp1g0t/providers/coingecko"
	"gitlab.com/Evrynet/sp1g0t/providers/gateio"
	"gitlab.com/Evrynet/sp1g0t/testing_context"
	"gitlab.com/Evrynet/sp1g0t/utils"
	"gorm.io/gorm"
)

type constraintCalculationTestCase struct {
	marketPrice decimal.Decimal //from coingecko or gateio
	order       models.OrderData
	bot         Sp1g0tBot
	expected    interface{}
	debugMsg    string
}

type ConstraintTestSuite struct {
	suite.Suite
	db  *gorm.DB
	tx  *gorm.DB
	ctx context.Context

	bot      Sp1g0tBot
	repo     repository.Repository
	provider *providers.PriceProvider
	mock     hydra.HydraMock
}

func (suite *ConstraintTestSuite) SetupTest() {
	gormDB, err := db.New(db.GetDBAuthFromEnv())
	if err != nil {
		logrus.Fatal(err)
	}

	suite.db = gormDB
	suite.mock = hydra.HydraMock{}

	coingeckoMock := coingecko.NewCoingecko(&coingecko.CoingeckoApiMock{}, true)
	gateIOMock := gateio.NewGateIO(gateio.GateeIOApiMock{})
	suite.provider = providers.NewPriceProvider(coingeckoMock, gateIOMock)
}

func (suite *ConstraintTestSuite) BeforeTest(suiteName, testName string) {
	suite.tx = suite.db.Begin()
	suite.repo = repository.NewRepository(repository.NewOrderRepository(suite.tx), repository.NewSettlesRepository(suite.tx),
		repository.NewPricesRepository(suite.tx), repository.NewWalletBalancesRepository(suite.tx))
	suite.bot = NewSp1g0tBot(&suite.mock, nil, suite.repo, suite.provider, aws.Aws{})
	suite.ctx = context.WithValue(context.Background(), testing_context.ContextKeyDB, suite.tx)
}

func (suite *ConstraintTestSuite) AfterTest(suiteName, testName string) {
	suite.tx.Rollback()
}

func builTestdObj(price string, marketPrice int64, assetPair, side string, bot Sp1g0tBot, expected interface{}, debugMsg string) constraintCalculationTestCase {
	return constraintCalculationTestCase{
		order: models.OrderData{
			Price:  price,
			Market: assetPair,
			Side:   side,
		},
		marketPrice: decimal.NewFromInt(marketPrice),
		expected:    expected,
		debugMsg:    debugMsg,
		bot:         bot,
	}
}

func Test_getSM(t *testing.T) {
	testCases := []constraintCalculationTestCase{
		builTestdObj("1000", 1000, "BTC/USDT", "buy", Sp1g0tBot{
			BotConfig: &config.Sp1g0tConfig{
				SafetyMargin: decimal.NewFromInt(5),
				AssetPairCfgMap: map[string]*config.AssetPairCfg{
					"BTC/USDT": {
						SafetyMargin: decimal.NewFromInt(10),
					},
				},
			},
		}, decimal.NewFromInt(900), "Use asset specific SM #1"),

		builTestdObj("1000", 1000, "BTC/USDT", "sell", Sp1g0tBot{
			BotConfig: &config.Sp1g0tConfig{
				SafetyMargin: decimal.NewFromInt(5),
				AssetPairCfgMap: map[string]*config.AssetPairCfg{
					"BTC/USDT": {
						SafetyMargin: decimal.NewFromInt(10),
					},
				},
			},
		}, decimal.NewFromInt(1100), "Use asset specific SM #2"),

		builTestdObj("1000", 1000, "BTC/USDT", "sell", Sp1g0tBot{
			BotConfig: &config.Sp1g0tConfig{
				SafetyMargin: decimal.NewFromInt(5),
			},
		}, decimal.NewFromInt(1050), "Use global SM 3"),

		builTestdObj("1000", 1000, "BTC/USDT", "buy", Sp1g0tBot{
			BotConfig: &config.Sp1g0tConfig{
				SafetyMargin: decimal.NewFromInt(5),
			},
		}, decimal.NewFromInt(950), "Use global SM #4"),
	}
	for _, v := range testCases {
		smPrice, err := v.bot.getSM(v.marketPrice, v.order.Market, v.order.Side)
		assert.Nil(t, err)
		assert.True(t, v.expected.(decimal.Decimal).Equal(smPrice), v.debugMsg)
	}
}

func Test_isFOinRange(t *testing.T) {
	testCases := []constraintCalculationTestCase{
		builTestdObj("1300", 1200, "BTC/USDT", "sell", Sp1g0tBot{
			BotConfig: &config.Sp1g0tConfig{
				BidMargin: decimal.NewFromInt(5),
				AskMargin: decimal.NewFromInt(6),
				AssetPairCfgMap: map[string]*config.AssetPairCfg{
					"BTC/USDT": {
						BidMargin: decimal.NewFromInt(9),
						AskMargin: decimal.NewFromInt(5),
					},
				},
			},
		}, false, "Use asset specific SM #1"), // 1200<1300<1200+5%

		builTestdObj("1000", 1100, "BTC/USDT", "buy", Sp1g0tBot{
			BotConfig: &config.Sp1g0tConfig{
				BidMargin: decimal.NewFromInt(6),
				AskMargin: decimal.NewFromInt(4),
				AssetPairCfgMap: map[string]*config.AssetPairCfg{
					"BTC/USDT": {
						BidMargin: decimal.NewFromInt(11),
						AskMargin: decimal.NewFromInt(10),
					},
				},
			},
		}, true, "Use asset specific SM #2"), // 1100>1000>1100-11%

		builTestdObj("1160", 1100, "BTC/USDT", "sell", Sp1g0tBot{
			BotConfig: &config.Sp1g0tConfig{
				BidMargin: decimal.NewFromInt(6),
				AskMargin: decimal.NewFromInt(5),
				AssetPairCfgMap: map[string]*config.AssetPairCfg{
					"SOL/USDT": {
						BidMargin: decimal.NewFromInt(11),
						AskMargin: decimal.NewFromInt(5),
					},
				},
			},
		}, false, "Use global SM #3"), // 1100<1160<1100+5%

		builTestdObj("1000", 1200, "BTC/USDT", "buy", Sp1g0tBot{
			BotConfig: &config.Sp1g0tConfig{
				BidMargin: decimal.NewFromInt(10),
				AskMargin: decimal.NewFromInt(5),
				AssetPairCfgMap: map[string]*config.AssetPairCfg{
					"SOL/USDT": {
						BidMargin: decimal.NewFromInt(11),
						AskMargin: decimal.NewFromInt(5),
					},
				},
			},
		}, false, "Use global SM #4"), // 1200>1000>1200-11%
	}
	for _, v := range testCases {
		ok, err := v.bot.isFOinRange(v.marketPrice, v.order.Price, v.order.Market, v.order.Side)
		assert.Nil(t, err)
		assert.Equal(t, v.expected.(bool), ok, v.debugMsg)
	}
}

func (suite *ConstraintTestSuite) Test_getFADandFBD() {
	ordersData := []models.OrderData{
		{
			Id:            uuid.New().String(),
			ClientId:      "client1",
			MarketAddress: "address1",
			Price:         "1020",
			Side:          "sell",
			Size:          "1",
			Market:        "BTC/USDT",
			Status:        utils.ORDER_STATUS_OPEN,
		},
		{
			Id:            uuid.New().String(),
			ClientId:      "client1",
			MarketAddress: "address1",
			Price:         "1030",
			Side:          "sell",
			Size:          "1",
			Market:        "BTC/USDT",
			Status:        utils.ORDER_STATUS_OPEN,
		},
	}

	for _, od := range ordersData {
		repository.GivenAnOrder().OrderData(od).Build(suite.ctx)
	}

	suite.bot.BotConfig = &config.Sp1g0tConfig{
		AssetPairCfgMap: map[string]*config.AssetPairCfg{
			"BTC/USDT": {
				BidMargin: decimal.NewFromInt(4),
				AskMargin: decimal.NewFromInt(8),
			},
		},
	}

	askDepth, bidDepth, err := suite.bot.getFADandFBD("address1", "BTC/USDT", decimal.NewFromInt(1000))
	if err != nil {
		suite.Failf("error getFad: %s", err.Error())
	}
	suite.Equal(decimal.NewFromInt(6330), askDepth, fmt.Sprintf("expected = %v, result = %v", decimal.NewFromInt(6330), askDepth))
	suite.Equal(decimal.NewFromInt(4950), bidDepth, fmt.Sprintf("expected = %v, result = %v", decimal.NewFromInt(4950), bidDepth))
}

func (suite *ConstraintTestSuite) Test_getOBG() {
	obg := suite.bot.getOBG(decimal.NewFromInt(15), decimal.NewFromInt(10))
	suite.Equal(decimal.NewFromInt(5), obg)

	obg = suite.bot.getOBG(decimal.NewFromInt(20), decimal.NewFromInt(10))
	suite.Equal(decimal.NewFromInt(10), obg)

	obg = suite.bot.getOBG(decimal.NewFromInt(15), decimal.NewFromInt(30))
	suite.Equal(decimal.NewFromInt(15), obg)
}

func (suite *ConstraintTestSuite) Test_getOBGwithinTolerance() {
	//(1-10/20)*100 - asset specific OBGT
	suite.bot.BotConfig = &config.Sp1g0tConfig{
		OrderBookGapTolerance: decimal.NewFromInt(60),
		AssetPairCfgMap: map[string]*config.AssetPairCfg{
			"BTC/USDT": {
				OrderBookGapTolerance: decimal.NewFromInt(60),
			},
		},
	}
	isInOBGwithinTolerance := suite.bot.getOBGwithinTolerance(decimal.NewFromInt(20), decimal.NewFromInt(10), "BTC/USDT")
	suite.Equal(true, isInOBGwithinTolerance)

	//(1-7/8)*100 - asset specific OBGT
	suite.bot.BotConfig = &config.Sp1g0tConfig{
		OrderBookGapTolerance: decimal.NewFromInt(60),
		AssetPairCfgMap: map[string]*config.AssetPairCfg{
			"BTC/USDT": {
				OrderBookGapTolerance: decimal.NewFromInt(10),
			},
		},
	}
	isInOBGwithinTolerance = suite.bot.getOBGwithinTolerance(decimal.NewFromInt(7), decimal.NewFromInt(8), "BTC/USDT")
	suite.Equal(false, isInOBGwithinTolerance)

	//(1-4/9)*100  - global OBGT
	suite.bot.BotConfig = &config.Sp1g0tConfig{
		OrderBookGapTolerance: decimal.NewFromInt(40),
		AssetPairCfgMap: map[string]*config.AssetPairCfg{
			"SOL/USDT": {
				OrderBookGapTolerance: decimal.NewFromInt(2),
			},
		},
	}
	isInOBGwithinTolerance = suite.bot.getOBGwithinTolerance(decimal.NewFromInt(9), decimal.NewFromInt(4), "BTC/USDT")
	suite.Equal(false, isInOBGwithinTolerance)

	//(1-4/9)*100 - global OBGT
	suite.bot.BotConfig = &config.Sp1g0tConfig{
		OrderBookGapTolerance: decimal.NewFromInt(60),
		AssetPairCfgMap: map[string]*config.AssetPairCfg{
			"SOL/USDT": {
				OrderBookGapTolerance: decimal.NewFromInt(2),
			},
		},
	}
	isInOBGwithinTolerance = suite.bot.getOBGwithinTolerance(decimal.NewFromInt(9), decimal.NewFromInt(4), "BTC/USDT")
	suite.Equal(true, isInOBGwithinTolerance)
}

func (suite *ConstraintTestSuite) Test_getOOROO() {
	order := repository.Order{
		OrderData: models.OrderData{
			Side:   "buy",
			Price:  "100",
			Market: "BTC/USDT",
		},
	}
	//90<100<110
	isInOOROO := suite.bot.getOOROO(decimal.NewFromInt(90), decimal.NewFromInt(110), order)
	suite.Equal(false, isInOOROO)

	//110<100<130
	isInOOROO = suite.bot.getOOROO(decimal.NewFromInt(110), decimal.NewFromInt(130), order)
	suite.Equal(true, isInOOROO)

	order = repository.Order{
		OrderData: models.OrderData{
			Side:   "sell",
			Price:  "100",
			Market: "BTC/USDT",
		},
	}
	//110>100>90
	isInOOROO = suite.bot.getOOROO(decimal.NewFromInt(90), decimal.NewFromInt(110), order)
	suite.Equal(false, isInOOROO)

	//120>100>110
	isInOOROO = suite.bot.getOOROO(decimal.NewFromInt(110), decimal.NewFromInt(120), order)
	suite.Equal(true, isInOOROO)
}

func (suite *ConstraintTestSuite) Test_checkBlacklisted() {
	suite.bot.BotConfig = &config.Sp1g0tConfig{
		AssetBlackList: "BTC,SOL",
	}

	isBlackListed := suite.bot.checkBlackisted("BTC/USDT")
	suite.True(isBlackListed)

	isBlackListed = suite.bot.checkBlackisted("DOGE/BTC")
	suite.True(isBlackListed)

	isBlackListed = suite.bot.checkBlackisted("DOGE/USDT")
	suite.False(isBlackListed)
}

func (suite *ConstraintTestSuite) Test_getSettlementTreshold() {
	suite.bot.BotConfig = &config.Sp1g0tConfig{
		SettlementThreshold: decimal.NewFromInt(60),
		AssetCfgMap: map[string]*config.AssetCfg{
			"SOL": {
				SettlementThreshold: decimal.NewFromInt(100),
			},
		},
	}

	settlementThreshold := suite.bot.getSettlementTreshold("SOL/USDT")
	suite.Equal(decimal.NewFromInt(100), settlementThreshold)

	suite.bot.BotConfig = &config.Sp1g0tConfig{
		SettlementThreshold: decimal.NewFromInt(60),
		AssetCfgMap: map[string]*config.AssetCfg{
			"BTC": {
				SettlementThreshold: decimal.NewFromInt(100),
			},
		},
	}

	settlementThreshold = suite.bot.getSettlementTreshold("SOL/USDT")
	suite.Equal(decimal.NewFromInt(60), settlementThreshold)

	suite.bot.BotConfig = &config.Sp1g0tConfig{
		SettlementThreshold: decimal.NewFromInt(160),
	}

	settlementThreshold = suite.bot.getSettlementTreshold("BTC/USDT")
	suite.Equal(decimal.NewFromInt(160), settlementThreshold)
}

func (suite *ConstraintTestSuite) Test_checkWalletBalance() {
	suite.bot.BotConfig = &config.Sp1g0tConfig{
		MinBalance: decimal.NewFromInt(20),
		AssetCfgMap: map[string]*config.AssetCfg{
			"RAY": {
				MinBalance: decimal.NewFromInt(150),
			},
		},
	}

	balances := []models.BalanceData{
		{
			Asset:    "BTC",
			MintAddr: "some_mint_addr2",
			Total:    "300",
		},
		{
			Asset:    "RAY",
			MintAddr: "some_mint_addr",
			Total:    "100",
		},
	}

	//use general MinBalance. 300 > 20
	isEnoughBalance, err := suite.bot.checkWalletBalance("BTC/USDT", "some_mint_addr2", balances)
	suite.Nil(err)
	suite.True(isEnoughBalance)

	//use asset speecific. 100 < 150
	isEnoughBalance, err = suite.bot.checkWalletBalance("RAY/USDC", "some_mint_addr", balances)
	suite.Nil(err)
	suite.False(isEnoughBalance)
}

func (suite *ConstraintTestSuite) Test_getDoNotTradePeriod() {
	suite.bot.BotConfig = &config.Sp1g0tConfig{
		DoNotTradePeriods: []config.NotTradePeriod{
			{
				From: time.Date(2022, 10, 1, 10, 0, 0, 0, time.UTC),
				To:   time.Date(2022, 10, 1, 15, 0, 0, 0, time.UTC),
			},
			{
				From: time.Date(2022, 11, 13, 12, 0, 0, 0, time.UTC),
				To:   time.Date(2022, 11, 13, 13, 0, 0, 0, time.UTC),
			},
		},
	}
	notTradeDuration, isDoNotTradePeriod := suite.bot.getDoNotTradePeriod(time.Date(2022, 10, 1, 11, 0, 0, 0, time.UTC))
	suite.True(isDoNotTradePeriod)
	suite.Equal(time.Duration(time.Hour*4), notTradeDuration)

	notTradeDuration, isDoNotTradePeriod = suite.bot.getDoNotTradePeriod(time.Date(2022, 11, 13, 14, 0, 0, 0, time.UTC))
	suite.False(isDoNotTradePeriod)
	suite.Zero(notTradeDuration)
}

func (suite *ConstraintTestSuite) Test_checkVolatilityTreshold() {
	dateNow := time.Date(2022, 1, 1, 11, 0, 0, 0, time.UTC)

	repository.GivenAPrice().Asset("SOL/USDT").Price("100").CreatedAt(time.Date(2022, 1, 1, 10, 30, 0, 0, time.UTC)).Build(suite.ctx)
	repository.GivenAPrice().Asset("SOL/USDT").Price("110").CreatedAt(time.Date(2022, 1, 1, 10, 35, 0, 0, time.UTC)).Build(suite.ctx)
	repository.GivenAPrice().Asset("SOL/USDT").Price("115").CreatedAt(time.Date(2022, 1, 1, 10, 39, 0, 0, time.UTC)).Build(suite.ctx)

	suite.bot.BotConfig = &config.Sp1g0tConfig{
		VolatilityPeriod:    time.Duration(1 * time.Hour),
		VolatilityThreshold: decimal.NewFromInt(15),
		AssetCfgMap: map[string]*config.AssetCfg{
			"SOL": {
				VolatilityPeriod:    time.Duration(30 * time.Minute),
				VolatilityThreshold: decimal.NewFromInt(5),
			},
		},
	}

	//deviation = 23.07692307692308 -> 23 > 5
	isVolatile := suite.bot.volatilityTooHigh(decimal.NewFromInt(130), "SOL/USDT", dateNow)
	suite.True(isVolatile)

	//deviation = 2.04081632653061 -> 2 < 5
	isVolatile = suite.bot.volatilityTooHigh(decimal.NewFromInt(98), "SOL/USDT", dateNow)
	suite.False(isVolatile)
}

func (suite *ConstraintTestSuite) Test_calculateWalletValue() {
	balances := []models.BalanceData{
		{
			Asset:    "bitcoin",
			MintAddr: "bitcoin_addr",
			Total:    "2",
		},
		{
			Asset:    "solana",
			MintAddr: "solana_addr",
			Total:    "10",
		},
	}

	markets := []models.MarketData{
		{
			BaseMintAddr:  "bitcoin_addr",
			Base:          "BTC",
			Quote:         "USDT",
			MarketAddress: "bitcoin_address",
		},
		{
			BaseMintAddr:  "solana_addr",
			Base:          "SOL",
			Quote:         "USDT",
			MarketAddress: "solana_address",
		},
	}

	//btc = 11236.25 * 2
	//sol = 162.5 * 10
	res := suite.bot.calculateWalletValue(balances, markets)
	suite.True(res.Equal(decimal.NewFromFloat(24097.5)))
}

func (suite *ConstraintTestSuite) Test_getOrderSize() {
	suite.bot.BotConfig = &config.Sp1g0tConfig{
		MaxOrderSize: decimal.NewFromFloat(2),
	}

	orderSize := suite.bot.getOrderSize(
		suite.bot.getOBG(decimal.NewFromFloat(14.3), decimal.NewFromFloat(15.7)),
		suite.bot.BotConfig.GetMaxOrderSize("BTC/USDT"),
		"0.001")
	suite.True(orderSize.Equal(decimal.NewFromFloat(1.4)))

	suite.bot.BotConfig = &config.Sp1g0tConfig{
		MaxOrderSize: decimal.NewFromFloat(2.5),
	}
	orderSize = suite.bot.getOrderSize(
		suite.bot.getOBG(decimal.NewFromFloat(14.9), decimal.NewFromFloat(14.99999999)),
		suite.bot.BotConfig.GetMaxOrderSize("BTC/USDT"),
		"0.1")
	suite.True(orderSize.Equal(decimal.NewFromFloat(0.1)))
}

func TestConstraintTestSuite(t *testing.T) {
	suite.Run(t, new(ConstraintTestSuite))
}
