package bot

import (
	"fmt"
	"strings"

	"github.com/shopspring/decimal"
	"github.com/sirupsen/logrus"
)

func (b Sp1g0tBot) GetPriceForAssetPair(assetPair, marketAddress string) (*decimal.Decimal, error) {
	providerPrice, err := b.getFromProviders(assetPair)
	if err != nil {
		return nil, err
	}

	latestPrice, err := b.getLatestTradePrice(assetPair, marketAddress)
	if err != nil {
		return nil, err
	}
	logrus.Infof("latestPrice for %s is %v", assetPair, latestPrice)

	if providerPrice.IsZero() {
		logrus.Info("price from providers is zero, so taking latestPrice")
		return &latestPrice, nil
	}

	avg := getAverage(providerPrice, latestPrice)
	logrus.Infof("average price between latestPrice and avgProviderPrice for %s is %v", assetPair, avg)
	return &avg, nil
}

func (b Sp1g0tBot) getFromProviders(assetPair string) (decimal.Decimal, error) {
	assetPairs := strings.Split(assetPair, "/")
	if len(assetPairs) != 2 {
		err := fmt.Errorf("invalid asset pair: '%v'", assetPair)
		logrus.Error(err)
		return decimal.Decimal{}, err
	}
	base := assetPairs[0]
	quote := assetPairs[1]

	avgBasePrice, err := b.getAvgUSDPriceFromProviders(base)
	if err != nil {
		return decimal.Decimal{}, err
	}
	logrus.Infof("average %s price is %v", base, avgBasePrice)

	avgQuotePrice, err := b.getAvgUSDPriceFromProviders(quote)
	if err != nil {
		return decimal.Decimal{}, err
	}
	logrus.Infof("average %s price is %v", quote, avgQuotePrice)

	//we need to avoid division on 0
	if avgQuotePrice.IsZero() {
		logrus.Infof("quote price for %s is zero, taking base price", quote)
		return avgBasePrice, nil
	}
	avgPrice := avgBasePrice.Div(avgQuotePrice)
	logrus.Infof("average price from coingecko+gateio for %s is %v", assetPair, avgPrice)
	return avgPrice, nil
}

func (b Sp1g0tBot) getAvgUSDPriceFromProviders(asset string) (decimal.Decimal, error) {
	coingeckoPrice, err := b.PriceProvider.Coingecko.GetUsdPrice(asset)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"method":   "GetAvgUSDPriceFromProviders",
			"provider": "coingecko",
			"call":     "GetUsdPrice",
			"asset":    asset,
		}).Errorf("%s", err.Error())
	}
	logrus.Infof("coingeckoPrice for %s is %v", asset, coingeckoPrice)

	gateIOPrice, err := b.PriceProvider.GateIO.GetUsdPrice(asset)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"method":   "GetAvgUSDPriceFromProviders",
			"provider": "gateio",
			"call":     "GetUsdPrice",
			"asset":    asset,
		}).Errorf("%s", err.Error())
	}
	logrus.Infof("gateIOPrice for %s is %v", asset, gateIOPrice)

	return getAverage(coingeckoPrice, gateIOPrice), nil
}

func (b Sp1g0tBot) getLatestTradePrice(assetPair, marketAddress string) (decimal.Decimal, error) {
	latestTrades, err := b.GetTheLatestTrade(marketAddress)
	if err != nil {
		return decimal.Decimal{}, err
	}
	if len(latestTrades.Trades) != 0 {
		price, err := decimal.NewFromString(latestTrades.Trades[0].Price)
		if err != nil {
			return decimal.Decimal{}, err
		}
		return price, nil
	}
	return decimal.Decimal{}, nil
}

// getAveragePrice calculates average for non-zero prices
func getAverage(prices ...decimal.Decimal) decimal.Decimal {
	var res []decimal.Decimal
	for _, p := range prices {
		if p.IsZero() {
			continue
		}
		res = append(res, p)
	}
	if len(res) == 0 {
		logrus.Error("empty prices from providers")
		return decimal.Decimal{}
	}

	return decimal.Avg(res[0], res[1:]...)
}
