package bot

import (
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/shopspring/decimal"
	"gitlab.com/Evrynet/sp1g0t/db/repository"
	"gitlab.com/Evrynet/sp1g0t/models"
)

func (b Sp1g0tBot) GetLocalOrder(id string) (order models.OrderResult, err error) {
	orderRepo, err := b.Repository.Order.GetOrder(id)
	if err != nil {
		return models.OrderResult{}, err
	}
	order.Orders = append(order.Orders, orderRepo.OrderData)
	return
}

func (b Sp1g0tBot) SaveOrderToLocal(orders []models.OrderData, tx models.SignatureReq) error {
	for _, o := range orders {
		order := orderToRepoOrder(o, tx.TxType, tx.TxId)
		err := b.Repository.Order.SaveOrder(order)
		if err != nil {
			return err
		}
	}
	return nil
}

func (b Sp1g0tBot) CancelLocalOrder(id, txID string) error {
	return b.Repository.Order.CancelOrder(id, txID)
}

func (b Sp1g0tBot) GetOrdersByStatus(status string) ([]repository.Order, error) {
	orders, err := b.Repository.Order.GetOrdersByStatus(strings.ToUpper(status))
	if err != nil {
		return nil, err
	}
	return orders, nil
}

func (b Sp1g0tBot) SaveSettleToLocal(market, marketAddress string, tx models.SignatureReq) error {
	return b.Repository.Settle.SaveSettle(repository.Settle{
		ID:            uuid.New(),
		TxID:          tx.TxId,
		TxType:        tx.TxType,
		Market:        market,
		MarketAddress: marketAddress,
		ClientID:      tx.ClientId,
	})
}

func (b Sp1g0tBot) SaveWalletBalancesToLocal(balances []models.BalanceData, now time.Time) error {
	for _, balance := range balances {
		err := b.Repository.WalletBalance.SaveWalletBalance(balanceDataToRepo(balance, now))
		if err != nil {
			return err
		}
	}
	return nil
}

func (b Sp1g0tBot) GetOwnOrdersForMarket(marketAddress string) ([]repository.Order, error) {
	return b.Repository.Order.GetOwnOrdersForMarket(marketAddress)
}

func (b Sp1g0tBot) GetMinPriceForPeriod(assetCode string, volatilityPeriod time.Duration, dateNow time.Time) (string, error) {
	return b.Repository.Price.GetMinPriceForPeriod(assetCode, volatilityPeriod, dateNow)
}

func (b Sp1g0tBot) SaveAssetPrice(asset string, marketPrice decimal.Decimal) error {
	return b.Repository.Price.SaveAssetPrice(&repository.AssetPrice{
		ID:       uuid.New(),
		ClientID: b.BotConfig.WalletAddress,
		Asset:    asset,
		Price:    marketPrice.String(),
	})
}

func (b Sp1g0tBot) GetWalletBalancesForPeriod(lossPeriod float64, dateNow time.Time) ([]repository.WalletBalance, error) {
	return b.Repository.WalletBalance.GetWalletBalancesForPeriod(lossPeriod, dateNow)
}
