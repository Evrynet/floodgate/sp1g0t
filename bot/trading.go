package bot

import (
	"fmt"
	"os"
	"sync"
	"time"

	"github.com/shopspring/decimal"
	"github.com/sirupsen/logrus"
	"gitlab.com/Evrynet/sp1g0t/db/repository"
	"gitlab.com/Evrynet/sp1g0t/models"
	"gitlab.com/Evrynet/sp1g0t/utils"
)

var wg sync.WaitGroup

func (b Sp1g0tBot) Start() {
	b.startTrading(time.Now().UTC())
	for {
		<-time.After(b.BotConfig.LoopPauseDuration)
		wg.Add(1)
		go func() {
			defer wg.Done()
			b.startTrading(time.Now().UTC())
		}()
		wg.Wait()
	}
}

func (b Sp1g0tBot) startTrading(now time.Time) {
	// the bot shall cancel all open orders before entering the "no trade" period.
	b.checkDoNotTradePeriod(now)

	logrus.Info("Getting info about balances...")
	balances, err := b.GetWalletBalances()
	if err != nil {
		return
	}

	logrus.Info("Update local balances...")
	err = b.SaveWalletBalancesToLocal(balances.Balances, now)
	if err != nil {
		logrus.Errorf("failed to update local balances. err = %s", err)
		return
	}

	//filter markets to trade only on markets where we have balances
	marketsForTrading := b.filterMarketsForTrading(balances.Balances)
	if marketsForTrading == nil {
		logrus.Warn("No markets for trading...")
		return
	}
	logrus.Infof("Found %d markets for trading", len(marketsForTrading))

	isPortfoliOk := b.checkPortfolio(now, marketsForTrading, balances.Balances)
	if !isPortfoliOk {
		logrus.Warnf("Portfolio with losses. Pause for %+vs", b.BotConfig.LossPeriod)
		//should take a break for LOSS_PERIOD and pause trading
		//cancel all own order in all markets
		b.cancelOwnOpenOrders()

		time.Sleep(time.Duration(b.BotConfig.LossPeriod) * time.Second)
		b.Start()
	}

	for _, m := range marketsForTrading {
		//skip blacklisted markets
		// xxx/BTC and BTC/xxx
		if b.checkBlackisted(m.Name) {
			continue
		}

		logrus.Infof("Starting to process market %s", m.Name)
		marketPrice, err := b.GetPriceForAssetPair(m.Name, m.MarketAddress)
		if err != nil || marketPrice.IsZero() {
			logrus.Warnf("For market %s average market price is zero. Canceling orders for this market...", m.Name)
			b.cancelAllOwnOrders(m.MarketAddress)
			continue
		}

		err = b.saveMarketPrice(m.Name, *marketPrice)
		if err != nil {
			continue
		}

		if b.volatilityTooHigh(*marketPrice, m.Name, time.Now().UTC()) {
			logrus.Warnf("Volatility for market %s too high, canceling own orders to prevent losses", m.Name)
			//cancel open orders and stop trading
			b.cancelAllOwnOrders(m.MarketAddress)
			//stop trading -> go to next market
			continue
		}

		err = b.checkSettlementTreshold(m.MarketAddress, m.Name, *marketPrice)
		if err != nil {
			continue
		}

		//1. cancel own orders which are out of range
		b.cancelOwnOutOfRangeOrders(m.MarketAddress, *marketPrice)

		//2.compute the FAD/FBD for asset pair
		fad, fbd, err := b.getFADandFBD(m.MarketAddress, m.Name, *marketPrice)
		if err != nil {
			continue
		}
		//if both are 0 then switch to another market
		if fad.IsZero() && fbd.IsZero() {
			logrus.Warnf("FAD and FBD are zeroes, so skipping market %s", m.Name)
			continue
		}

		//3. OBG within tolerance?
		isWithinTolerance := b.getOBGwithinTolerance(fad, fbd, m.Name)
		if isWithinTolerance {
			continue
		}

		isEnoughBalance, err := b.checkWalletBalance(m.Name, m.BaseMintAddr, balances.Balances)
		if err != nil {
			continue
		}
		if !isEnoughBalance {
			logrus.Warnf("not enough balance for %s", m.Name)

			ots := utils.OkayToSendWarning()
			logrus.Infof("> OkayToSendWarning returned: %v", ots)

			if ots {
				err := b.AwsService.Smtp.Send(b.BotConfig.Email, "Wallet balance", fmt.Sprintf("[%s] Not enough balance to trade on %s", os.Getenv("DEPLOYMENT_ENV"), m.Name))
				if err != nil {
					logrus.Errorf("failed to send email. err = %s", err)
				}
			}
			continue
		}

		orderSize := b.getOrderSize(b.getOBG(fad, fbd), b.BotConfig.GetMaxOrderSize(m.Name), m.MinOrderSize)
		if fad.GreaterThan(fbd) {
			//6. reduce OAD and increase OBD until equilibrium is reached (place buy orders)
			b.equalizeEquilibrium(m.Name, m.MarketAddress, utils.SIDE_BUY, m.TickSize, orderSize, *marketPrice)
		} else {
			//7.reduce OBD and increase OAD until equilibrium is reached (place sell orders)
			b.equalizeEquilibrium(m.Name, m.MarketAddress, utils.SIDE_SELL, m.TickSize, orderSize, *marketPrice)
		}

		logrus.Infof("Market %s is done, going to the next one", m.Name)
	}
}

//cancelOwnOrders check orders from local db (own orders) and cancel them returning  ids
func (b Sp1g0tBot) cancelOwnOutOfRangeOrders(marketAddress string, marketPrice decimal.Decimal) error {
	ownOrders, err := b.GetOwnOrdersForMarket(marketAddress)
	if err != nil {
		logrus.Error("cancel OwnOutOfRange Orders err. ", err)
		return err
	}

	for _, order := range ownOrders {
		orderSMprice, err := b.getSM(marketPrice, order.Market, order.Side)
		if err != nil {
			continue
		}

		isOROO := b.getOOROO(orderSMprice, marketPrice, order)
		if isOROO {
			_, err := b.CancelOrder(order.Id)
			if err != nil {
				logrus.Error("error canceling OROO order. ", err)
				continue
			}
		}
	}
	return nil
}

func (b Sp1g0tBot) equalizeEquilibrium(marketName, marketAddress, side, tickSize string, orderSize, marketPrice decimal.Decimal) error {
	orderPrice, err := b.getSM(marketPrice, marketName, side)
	if err != nil {
		return err
	}
	tickSizeDecimal, err := decimal.NewFromString(tickSize)
	if err != nil {
		logrus.Error(err)
		return err
	}
	logrus.Infof("Preparing order to equalize equilibrium. marketName=%s, marketAddres=%s, side=%s, orderSize=%v, marketPrice=%v",
		marketName, marketAddress, side, orderSize, marketPrice)
	if side == utils.SIDE_BUY {
		marketPrice = marketPrice.Sub(marketPrice.Mod(tickSizeDecimal))
	} else if side == utils.SIDE_SELL {
		marketPrice = utils.FindTheClosestBiggestWithoutReminder(marketPrice, tickSizeDecimal)
	}

	_, err = b.PlaceOrder(b.BotConfig.WalletAddress, marketName, marketAddress, orderPrice.String(), side, orderSize.String(), "limit")
	if err != nil {
		return err
	}
	return nil
}

func (b Sp1g0tBot) getWalletBalancesForLossPeriod(dateNow time.Time) ([]repository.WalletBalance, error) {
	lossPeriod := b.BotConfig.GetLossPeriod()
	walletBalances, err := b.GetWalletBalancesForPeriod(lossPeriod, dateNow)
	if err != nil {
		return nil, err
	}

	return walletBalances, nil
}

func (b Sp1g0tBot) checkPortfolio(now time.Time, markets []models.MarketData, currentBalances []models.BalanceData) bool {
	localBalances, err := b.getWalletBalancesForLossPeriod(now)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"method": "checkPortfolio",
			"call":   "getWalletBalancesForLossPeriod",
		}).Errorf("failed to get wallet balances for loss period: %s", err.Error())
		return false
	}

	if localBalances == nil {
		err := b.SaveWalletBalancesToLocal(currentBalances, now)
		if err != nil {
			logrus.WithFields(logrus.Fields{
				"method": "checkPortfolio",
				"call":   "SaveWalletBalancesToLocal",
			}).Errorf("failed to save balances: %s", err.Error())
			return false
		}
	} else {
		diff := now.Sub(localBalances[0].CreatedAt).Seconds()
		if diff >= b.BotConfig.LossPeriod {
			currentPortfolio := b.calculateWalletValue(currentBalances, markets)

			var localBalancesToBalances []models.BalanceData
			for _, b := range localBalances {
				localBalancesToBalances = append(localBalancesToBalances, repoWalletBalanceToBalanceData(b))
			}
			previosPortfolio := b.calculateWalletValue(localBalancesToBalances, markets)

			logrus.Infof("currentPortfolio = %v, previosPortfolio = %v", currentPortfolio, previosPortfolio)
			return b.checkLossTreshold(currentPortfolio, previosPortfolio)
		}
	}
	return true
}

func (b Sp1g0tBot) filterMarketsForTrading(balances []models.BalanceData) (marketsForTrading []models.MarketData) {
	var (
		markets []models.MarketData
		err     error
	)
	if b.BotConfig.MarketsTradeOnly != nil {
		for _, v := range b.BotConfig.MarketsTradeOnly {
			marketsData, err := b.GetMarkets(v[0], v[1])
			if err != nil {
				return nil
			}
			markets = append(markets, marketsData...)
		}
	} else {
		markets, err = b.GetMarkets("", "")
		if err != nil {
			return nil
		}
	}

	for _, balance := range balances {
		for _, market := range markets {
			if balance.MintAddr == market.BaseMintAddr {
				marketsForTrading = append(marketsForTrading, market)
				break
			}
		}
	}
	return
}
