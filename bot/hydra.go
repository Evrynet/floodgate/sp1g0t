package bot

import (
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/Evrynet/sp1g0t/models"
)

func (b Sp1g0tBot) Register(req models.RegistrationReq) error {
	err := b.hydraAPI.Register(req)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"method": "Register",
			"hydra":  true,
		}).Errorf("failed to register. err = %s", err)
		return err
	}
	return nil
}

func (b Sp1g0tBot) Verify(token string) error {
	err := b.hydraAPI.Verify(token)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"method": "Verify",
			"hydra":  true,
		}).Errorf("failed to verify. err = %s", err)
		return err
	}
	return nil
}

func (b Sp1g0tBot) GetMarketPairs(basep, quotep string) ([]string, error) {
	marketPairs, err := b.hydraAPI.GetMarkets(basep, quotep)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"method": "GetMarketPairs",
			"hydra":  true,
			"basep":  basep,
			"quotep": quotep,
		}).Errorf("failed to fetch market pairs. err = %s", err)
		return nil, err
	}
	return marketPairs.Pairs, nil
}

func (b Sp1g0tBot) GetMarketPairData(pair string) (marketResult models.MarketResult, err error) {
	marketResult, err = b.hydraAPI.GetMarketPairData(strings.Replace(pair, "/", "", -1))
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"method": "GetMarketPairData",
			"hydra":  true,
			"pair":   pair,
		}).Errorf("failed to fetch market pairs data. err = %s", err)
		return
	}
	return
}

func (b Sp1g0tBot) GetOrder(id string, isLocal bool) (order models.OrderResult, err error) {
	if isLocal {
		order, err = b.GetLocalOrder(id)
		if err != nil {
			logrus.WithFields(logrus.Fields{
				"method":  "GetOrder",
				"hydra":   false,
				"isLocal": isLocal,
				"id":      id,
			}).Errorf("failed to get order. err = %s", err)
			return
		}
		return
	}
	order, err = b.hydraAPI.GetOrder(id)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"method":  "GetOrder",
			"hydra":   true,
			"isLocal": isLocal,
			"id":      id,
		}).Errorf("failed to get order. err = %s", err)
		return
	}
	return
}

func (b Sp1g0tBot) GetOrdersForMarket(marketAddress string) (orders models.OrderResult, err error) {
	orders, err = b.hydraAPI.GetOrdersForMarket(marketAddress)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"method":        "GetOrdersForMarket",
			"hydra":         true,
			"marketAddress": marketAddress,
		}).Errorf("failed to get orders for market. err = %s", err)
		return
	}
	return
}

func (b Sp1g0tBot) PlaceOrder(clientID, market, marketAddress, price, side, size, orderType string) (*models.SignedOrders, error) {
	req := models.OrderReq{
		ClientId:      clientID,
		Market:        market,
		MarketAddress: marketAddress,
		Price:         price,
		Side:          side,
		Size:          size,
		OrderType:     orderType,
	}
	tx, err := b.hydraAPI.PlaceOrder(req)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"method":        "PlaceOrder",
			"hydra":         true,
			"marketAddress": marketAddress,
			"market":        market,
			"price":         price,
			"side":          side,
			"size":          size,
		}).Errorf("failed to place order. err = %s", err)
		return nil, err
	}

	signedOrder, err := b.sign(tx.TxId, tx.TxData)
	if err != nil {
		return nil, err
	}

	err = b.SaveOrderToLocal(signedOrder.Orders, tx)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"method":  "SaveOrderToLocal",
			"hydra":   false,
			"isLocal": true,
		}).Errorf("failed to save order to local db. err = %s", err)
		return nil, err
	}

	return signedOrder, nil
}

func (b Sp1g0tBot) CancelOrder(id string) (*models.SignedOrders, error) {
	tx, err := b.hydraAPI.CancelOrder(id)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"method": "CancelOrder",
			"hydra":  true,
			"id":     id,
		}).Errorf("failed to cancel order. err = %s", err)
		return nil, err
	}
	canceledOrder, err := b.sign(tx.TxId, tx.TxData)
	if err != nil {
		return nil, err
	}

	err = b.CancelLocalOrder(id, tx.TxId)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"method":  "CancelLocalOrder",
			"hydra":   false,
			"isLocal": true,
		}).Errorf("failed to cancel order in local db. err = %s", err)
		return nil, err
	}

	return canceledOrder, nil
}

func (b Sp1g0tBot) Settle(market, marketAddress string) error {
	tx, err := b.hydraAPI.Settle(market, marketAddress)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"method":        "Settle",
			"hydra":         true,
			"market":        market,
			"marketAddress": marketAddress,
		}).Errorf("failed to settle. err = %s", err)
		return err
	}

	err = b.SaveSettleToLocal(market, marketAddress, tx)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"method":  "SaveSettleToLocal",
			"hydra":   false,
			"isLocal": true,
		}).Errorf("failed to save seettle info to local db. err = %s", err)
		return err
	}

	_, err = b.sign(tx.TxId, tx.TxData)
	if err != nil {
		return err
	}

	return nil
}

func (b Sp1g0tBot) sign(txID, txData string) (*models.SignedOrders, error) {
	body := b.hydraAPI.SignBody(txData)
	res, err := b.hydraAPI.Sign(txID, models.SignatureData{Signature: body})
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"method": "Sign",
			"hydra":  true,
			"txID":   txID,
		}).Errorf("failed to sign. err = %s", err)
		return nil, err
	}

	return &res, nil
}

func (b Sp1g0tBot) GetWalletBalances() (balances *models.WalletBalancesResult, err error) {
	balances, err = b.hydraAPI.GetWalletBalances()
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"method": "GetWalletBalances",
			"hydra":  true,
		}).Errorf("failed to fetch wallet balances. err = %s", err)
		return
	}
	return
}

func (b Sp1g0tBot) GetOrderBook(marketAddress string) (orderbook *models.OrderbookResult, err error) {
	orderbook, err = b.hydraAPI.GetOrderBook(marketAddress)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"method": "GetOrderBook",
			"hydra":  true,
		}).Errorf("failed to fetch orderbook. err = %s", err)
		return
	}
	return
}

func (b Sp1g0tBot) GetMarketBalances(marketAddress string) (balances *models.OpenBalancesResult, err error) {
	balances, err = b.hydraAPI.GetMarketBalances(marketAddress)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"method":        "GetMarketBalances",
			"hydra":         true,
			"marketAddress": marketAddress,
		}).Errorf("failed to fetch market balances. err = %s", err)
		return
	}
	return
}

func (b Sp1g0tBot) GetTheLatestTrade(marketAddress string) (trades *models.LatestTradesResult, err error) {
	trades, err = b.hydraAPI.GetTheLatestTrade(marketAddress)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"method":        "GetTheLatestTrade",
			"hydra":         true,
			"marketAddress": marketAddress,
		}).Errorf("failed to fetch latest trades. err = %s", err)
		return
	}
	return
}
