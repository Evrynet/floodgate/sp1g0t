package bot

import (
	"errors"
	"strings"
	"time"

	"github.com/shopspring/decimal"
	"github.com/sirupsen/logrus"
	"gitlab.com/Evrynet/sp1g0t/db/repository"
	"gitlab.com/Evrynet/sp1g0t/models"
	"gitlab.com/Evrynet/sp1g0t/utils"
)

// Get Safety Margin
func (b Sp1g0tBot) getSM(marketPrice decimal.Decimal, assetPair, side string) (decimal.Decimal, error) {
	var sm decimal.Decimal

	smValue := b.BotConfig.GetSafetyMarginValue(assetPair)

	switch {
	case side == utils.SIDE_SELL:
		sm = marketPrice.Add(percentageFromNumber(marketPrice, smValue))
	case side == utils.SIDE_BUY:
		sm = marketPrice.Sub(percentageFromNumber(marketPrice, smValue))
	default:
		logrus.WithFields(logrus.Fields{
			"method":      "getSM",
			"assetPair":   assetPair,
			"side":        side,
			"marketPrice": marketPrice,
		}).Errorf("failed to get SM. err = %s", "wrong side")
		return decimal.Decimal{}, errors.New("wrong side")
	}
	return sm, nil
}

// Get Foreign order in price range
func (b Sp1g0tBot) isFOinRange(marketPrice decimal.Decimal, orderPrice, market, side string) (bool, error) {
	bmValue := b.BotConfig.GetBidMarginValue(market)
	amValue := b.BotConfig.GetAskMarginValue(market)

	orderPriceDecimal, err := decimal.NewFromString(orderPrice)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"method":     "isFOinRange",
			"orderPrice": orderPrice,
		}).Errorf("failed to order price. err = %s", err)
		return false, err
	}

	switch {
	case side == utils.SIDE_BUY:
		return marketPrice.GreaterThan(orderPriceDecimal) && orderPriceDecimal.GreaterThan(marketPrice.Sub(percentageFromNumber(marketPrice, bmValue))), nil
	case side == utils.SIDE_SELL:
		return marketPrice.LessThan(orderPriceDecimal) && orderPriceDecimal.LessThan(marketPrice.Add(percentageFromNumber(marketPrice, amValue))), nil
	default:
		logrus.WithFields(logrus.Fields{
			"method":      "isFOinRange",
			"marketPrice": marketPrice,
			"orderPrice":  orderPrice,
			"market":      market,
			"side":        side,
		}).Errorf("failed to calculate isFOinRange. err = %s", "wrong side")
		return false, errors.New("wrong side")
	}
}

// ask = sell
// bid = buy
//FAD: foreign ask depth
//FBD: foreign bid depth
func (b Sp1g0tBot) getFADandFBD(marketAddress, market string, marketPrice decimal.Decimal) (decimal.Decimal, decimal.Decimal, error) {
	var fad, fbd decimal.Decimal

	filteredOrderbookAsks, filteredOrderbookBids, err := b.filterOrderbookInRange(marketAddress, market, marketPrice)
	if err != nil {
		return decimal.Decimal{}, decimal.Decimal{}, err
	}

	orderbookAskDepth, orderbookBidDepth, err := calculateOrderbookDepth(filteredOrderbookAsks, filteredOrderbookBids)
	if err != nil {
		return decimal.Decimal{}, decimal.Decimal{}, err
	}

	filteredOrders, err := b.filterOwnOrdersInRange(marketAddress, marketPrice)
	if err != nil {
		return decimal.Decimal{}, decimal.Decimal{}, err
	}
	ordersAskDepth, ordersBidDepth, err := calculateOwnOrdersDepth(filteredOrders)
	if err != nil {
		return decimal.Decimal{}, decimal.Decimal{}, err
	}

	fad = orderbookAskDepth.Sub(ordersAskDepth)
	logrus.Infof("FAD for market %s is %v", market, fad)
	fbd = orderbookBidDepth.Sub(ordersBidDepth)
	logrus.Infof("FBD for market %s is %v", market, fbd)

	return fad, fbd, nil
}

//OBG: order book gap
func (b Sp1g0tBot) getOBG(fad, fbd decimal.Decimal) decimal.Decimal {
	obg := fad.Sub(fbd).Abs()
	logrus.WithFields(logrus.Fields{
		"method": "getOBG",
		"fad":    fad,
		"fbd":    fbd,
	}).Infof("order book gap is %v", obg)
	return obg
}

//Order book gap is within tolerance when (1 - (min(FAD, FBD) / max(FAD, FBD))) * 100 < OBGT%
func (b Sp1g0tBot) getOBGwithinTolerance(fad, fbd decimal.Decimal, market string) bool {
	obgtValue := b.BotConfig.GetOBGTValue(market)
	//if FAD or FBD is 0, then OBGwithinTolerance value should be 100%
	if fad.IsZero() || fbd.IsZero() {
		logrus.WithFields(logrus.Fields{
			"method": "getOBGwithinTolerance",
			"fad":    fad,
			"fbd":    fbd,
			"market": market,
		}).Infof("FAD and FBD are zeroes. Compare obgtValue %v with 100percent", obgtValue)
		return decimal.NewFromInt(100).LessThan(obgtValue)
	}
	logrus.Infof("for market %s, OBG=%v, obgtValue=%v", market, (decimal.NewFromInt(1).
		Sub(
			(decimal.Min(fad, fbd)).
				Div(decimal.Max(fad, fbd)))).
		Mul(decimal.NewFromInt(100)), obgtValue)

	return (decimal.NewFromInt(1).
		Sub(
			(decimal.Min(fad, fbd)).
				Div(decimal.Max(fad, fbd)))).
		Mul(decimal.NewFromInt(100)).LessThan(obgtValue)
}

//OOR-OO: out of range own order
func (b Sp1g0tBot) getOOROO(smPrice, marketPrice decimal.Decimal, order repository.Order) bool {
	price, err := decimal.NewFromString(order.Price)
	if err != nil {
		return false
	}
	if !(price.GreaterThanOrEqual(decimal.Min(smPrice, marketPrice)) && price.LessThanOrEqual(decimal.Max(smPrice, marketPrice))) {
		return true
	} else {
		return false
	}
}

func (b Sp1g0tBot) checkBlackisted(marketName string) bool {
	for _, asset := range strings.Split(b.BotConfig.AssetBlackList, ",") {
		if strings.Contains(marketName, asset) {
			logrus.Infof("Market %s is blacklisted.", marketName)
			return true
		}
	}
	return false
}

func (b Sp1g0tBot) getSettlementTreshold(asset string) decimal.Decimal {
	return b.BotConfig.GetSettlementTreshold(asset)
}

func (b Sp1g0tBot) checkSettlementTreshold(marketAddress, asset string, marketPrice decimal.Decimal) error {
	settlementThreshold := b.getSettlementTreshold(asset)
	if marketPrice.GreaterThan(settlementThreshold) {
		logrus.Infof("For asset %s market price (%v) greater then SETTLMENT_THRESHOLD constraint. Settling...", asset, marketPrice)
		err := b.Settle(asset, marketAddress)
		if err != nil {
			return err
		}
	}
	return nil
}

//checkWalletBalance check if balance for given asset is bigger then MIN_BALANCE constraint
//returns `false` in case of any error and when MIN_BALANCE > asset amount
// returns true if MIN_BALANCE < asset amount
func (b Sp1g0tBot) checkWalletBalance(assetPair, mintAddress string, balances []models.BalanceData) (bool, error) {
	assets := strings.Split(assetPair, "/")
	base := assets[0]

	minBalance := b.BotConfig.GetMinBalance(base)

	for _, balance := range balances {
		if balance.MintAddr == mintAddress {
			totalAmount, err := decimal.NewFromString(balance.Total)
			if err != nil {
				logrus.Error(err)
				return false, err
			}
			if minBalance.GreaterThan(totalAmount) {
				return false, nil
			}
			return true, nil
		}
	}
	return false, nil
}

func (b Sp1g0tBot) getDoNotTradePeriod(now time.Time) (time.Duration, bool) {
	for _, t := range b.BotConfig.DoNotTradePeriods {
		if now.After(t.From) && now.Before(t.To) {
			notTradeDuration := t.To.Sub(now)
			return notTradeDuration, true
		}
		continue
	}
	return 0, false
}

// volatilityTooHigh check that deviation shouldnt be more then volatility_treshold
// if deviation > volatility_treshold return true, means too volatile and we will stop trading
// if deviation < volatility_treshold return false and means that we can continue trading
func (b Sp1g0tBot) volatilityTooHigh(marketPrice decimal.Decimal, assetPair string, dateNow time.Time) bool {
	volatilityThreshold := b.BotConfig.GetVolatilityThreshold(assetPair)
	volatilityPeriod := b.BotConfig.GetVolatilityPeriod(assetPair)

	price, err := b.GetMinPriceForPeriod(assetPair, volatilityPeriod, dateNow)
	if err != nil {
		logrus.Error(err)
		return false
	}

	priceDecimal, err := decimal.NewFromString(price)
	if err != nil {
		logrus.Error(err)
		return false
	}

	deviation := utils.PercentageDeviation(marketPrice, priceDecimal)
	logrus.WithFields(logrus.Fields{
		"asset":       assetPair,
		"marketPrice": marketPrice,
		"price":       priceDecimal,
	}).Infof("Volatility Treshold deviation is %v", deviation)
	return deviation.GreaterThan(volatilityThreshold)
}

func (b Sp1g0tBot) checkLossTreshold(currentBalance, oldBalance decimal.Decimal) bool {
	balanceDiff := currentBalance.Sub(oldBalance)
	logrus.WithFields(logrus.Fields{
		"currentBalance": currentBalance,
		"oldBalance":     oldBalance,
	}).Infof("Balance difference is %v", balanceDiff)

	return currentBalance.GreaterThanOrEqual(oldBalance) || balanceDiff.GreaterThan(b.BotConfig.LossThreshold)
}

func (b Sp1g0tBot) getOrderSize(obg, maxOrderSize decimal.Decimal, minOrderSize string) decimal.Decimal {
	minOrderSizeDecimal, err := decimal.NewFromString(minOrderSize)
	if err != nil {
		return decimal.Decimal{}
	}

	orderSize := decimal.Min(obg, maxOrderSize)

	logrus.WithFields(logrus.Fields{
		"minOrderSize": minOrderSizeDecimal,
		"obg":          obg,
		"maxOrderSize": maxOrderSize,
	}).Infof("orderSize is %v", orderSize)

	if orderSize.LessThan(minOrderSizeDecimal) {
		logrus.Infof("orderSize is less then minOrderSize. Using minOrderSize (%v) as orderSize", minOrderSizeDecimal)
		return minOrderSizeDecimal
	}

	return orderSize
}

func (b Sp1g0tBot) checkDoNotTradePeriod(now time.Time) {
	notTradeDuration, isDoNotTradePeriod := b.getDoNotTradePeriod(now)
	if isDoNotTradePeriod {
		b.cancelOwnOpenOrders()
		logrus.Warnf("Gonna freeze for %s because of not_to_trade_period", notTradeDuration)
		time.Sleep(notTradeDuration)
		logrus.Info("wake up, Neo, continue execution")
		return
	}
}
