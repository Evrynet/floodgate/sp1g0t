package bot

import (
	"github.com/shopspring/decimal"
	"github.com/sirupsen/logrus"
)

func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

//(number/100)*percentage
func percentageFromNumber(number, percentage decimal.Decimal) decimal.Decimal {
	return number.Div(decimal.NewFromInt(100)).Mul(percentage)
}

func getOrderDepth(size, price string) (decimal.Decimal, error) {
	sizeDecimal, err := decimal.NewFromString(size)
	if err != nil {
		logrus.Error(err)
		return decimal.Decimal{}, err
	}
	priceDecimal, err := decimal.NewFromString(price)
	if err != nil {
		logrus.Error(err)
		return decimal.Decimal{}, err
	}
	return sizeDecimal.Mul(priceDecimal), nil
}
