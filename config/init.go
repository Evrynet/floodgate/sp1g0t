package config

import (
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/shopspring/decimal"
	"github.com/sirupsen/logrus"
	"gitlab.com/Evrynet/sp1g0t/utils"
)

// InitConfigs read data from .env and build cfg object for bot execution
func InitConfigs(marketPairs []string) (*Sp1g0tConfig, error) {
	loopDuration, err := time.ParseDuration(utils.TrimSpacesFromEnv(os.Getenv(utils.LOOP_PAUSE_DURATION)))
	if err != nil {
		return nil, errors.New("wrong value for LOOP_PAUSE_DURATION")
	}

	orderHoldoffPeriod, err := strconv.Atoi(utils.TrimSpacesFromEnv(os.Getenv(utils.ORDER_HOLDOFF_PERIOD)))
	if err != nil {
		return nil, errors.New("wrong value for ORDER_HOLDOFF_PERIOD")
	}

	//global vars which are used if are not redifined for specific asset
	askMargin, err := decimal.NewFromString(utils.TrimSpacesFromEnv(os.Getenv(utils.ASK_MARGIN)))
	if err != nil {
		return nil, errors.New("wrong value for ASK_MARGIN")
	}
	bidMargin, err := decimal.NewFromString(utils.TrimSpacesFromEnv(os.Getenv(utils.BID_MARGIN)))
	if err != nil {
		return nil, errors.New("wrong value for BID_MARGIN")
	}
	safetyMargin, err := decimal.NewFromString(utils.TrimSpacesFromEnv(os.Getenv(utils.SAFETY_MARGIN)))
	if err != nil {
		return nil, errors.New("wrong value for SAFETY_MARGIN")
	}
	orderBookGapTolerance, err := decimal.NewFromString(utils.TrimSpacesFromEnv(os.Getenv(utils.ORDER_BOOK_GAP_TOLERANCE)))
	if err != nil {
		return nil, errors.New("wrong value for ORDER_BOOK_GAP_TOLERANCE")
	}
	settlementThreshold, err := decimal.NewFromString(utils.TrimSpacesFromEnv(os.Getenv(utils.SETTLEMENT_THRESHOLD)))
	if err != nil {
		return nil, errors.New("wrong value for ORDER_BOOK_GAP_TOLERANCE")
	}
	lowThreshold, err := decimal.NewFromString(utils.TrimSpacesFromEnv(os.Getenv(utils.LOW_THRESHOLD)))
	if err != nil {
		return nil, errors.New("wrong value for LOW_THRESHOLD")
	}
	maxOrderSize, err := decimal.NewFromString(utils.TrimSpacesFromEnv(os.Getenv(utils.MAX_ORDER_SIZE)))
	if err != nil {
		return nil, errors.New("wrong value for MAX_ORDER_SIZE")
	}
	minBalance, err := decimal.NewFromString(utils.TrimSpacesFromEnv(os.Getenv(utils.MIN_BALANCE)))
	if err != nil {
		return nil, errors.New("wrong value for MIN_BALANCE")
	}
	volatilityThreshold, err := decimal.NewFromString(utils.TrimSpacesFromEnv(os.Getenv(utils.VOLATILITY_THRESHOLD)))
	if err != nil {
		return nil, errors.New("wrong value for VOLATILITY_THRESHOLD")
	}
	volatilityPeriod, err := time.ParseDuration(utils.TrimSpacesFromEnv(os.Getenv(utils.VOLATILITY_PERIOD)))
	if err != nil {
		return nil, errors.New("wrong value for VOLATILITY_PERIOD")
	}
	lossThreshold, err := decimal.NewFromString(utils.TrimSpacesFromEnv(os.Getenv(utils.LOSS_THRESHOLD)))
	if err != nil {
		return nil, errors.New("wrong value for LOSS_THRESHOLD")
	}
	lossPeriod, err := time.ParseDuration(utils.TrimSpacesFromEnv(os.Getenv(utils.LOSS_PERIOD)))
	if err != nil {
		return nil, errors.New("wrong value for LOSS_PERIOD")
	}

	assetCfgMap := make(map[string]*AssetCfg)
	assetPairCfgMap := make(map[string]*AssetPairCfg)

	//checking should be optimized in the future
	for _, v := range marketPairs {
		base := strings.Split(v, "/")[0]

		assetCfg, err := checkAssetCfgEnvVars(base)
		//no configuration for asset presented in the market so skip
		if assetCfg == nil {
			continue
		}
		if err != nil {
			logrus.Error(err)
			continue
		}
		assetCfgMap[base] = assetCfg

		assetPair, err := checkAssetPairCfgEnvVars(v)
		if err != nil {
			logrus.Error(err)
			continue
		}
		if assetPair == nil {
			continue
		}
		assetPairCfgMap[v] = assetPair
	}

	notTradePeriods, err := parseDoNotTradePeriods()
	if err != nil {
		logrus.Error(err)
		return nil, err
	}

	botConfig := &Sp1g0tConfig{
		OrganizationName:      utils.TrimSpacesFromEnv(os.Getenv(utils.ORGANIZATION_NAME)),
		Email:                 utils.TrimSpacesFromEnv(os.Getenv(utils.EMAIL)),
		WalletAddress:         utils.TrimSpacesFromEnv(os.Getenv(utils.WALLET)),
		LoopPauseDuration:     loopDuration,
		AssetBlackList:        strings.ToUpper(utils.TrimSpacesFromEnv(os.Getenv(utils.ASSET_BLACK_LIST))),
		OrderHoldoffPeriod:    orderHoldoffPeriod,
		AskMargin:             askMargin,
		BidMargin:             bidMargin,
		SafetyMargin:          safetyMargin,
		OrderBookGapTolerance: orderBookGapTolerance,
		SettlementThreshold:   settlementThreshold,
		LowThreshold:          lowThreshold,
		MinBalance:            minBalance,
		MaxOrderSize:          maxOrderSize,
		AssetCfgMap:           assetCfgMap,
		AssetPairCfgMap:       assetPairCfgMap,
		DoNotTradePeriods:     notTradePeriods,
		VolatilityPeriod:      volatilityPeriod,
		VolatilityThreshold:   volatilityThreshold,
		LossPeriod:            lossPeriod.Seconds(),
		LossThreshold:         lossThreshold,
		MarketsTradeOnly:      buildTradeOnlyMarkets(utils.TrimSpacesFromEnv(os.Getenv(utils.ONLY_TRADE_THESE_PAIRS))),
	}

	err = utils.ValidateEmail(botConfig.Email)
	if err != nil {
		return nil, err
	}

	return botConfig, nil
}

func checkAssetPairCfgEnvVars(assetPair string) (*AssetPairCfg, error) {
	//asset specific vars (like BTC_USDC_AM, ETH_USDT_BM etc.)
	var (
		askMargin, bidMargin, safetyMargin, orderBookGapTolerance, maxOrderSize decimal.Decimal
		err                                                                     error
	)

	assetCode := strings.Replace(assetPair, "/", "_", -1)
	////
	// it is ok if some of specific vars are not presented,
	// so we just check not empty vars and parse them
	////
	if askMarginEnv, ok := os.LookupEnv(utils.TrimSpacesFromEnv(assetCode + "_AM")); ok {
		askMargin, err = decimal.NewFromString(askMarginEnv)
		if err != nil {
			return nil, fmt.Errorf("error with converting %s_AM", assetCode)
		}
	}

	if bidMarginEnv, ok := os.LookupEnv(utils.TrimSpacesFromEnv(assetCode + "_BM")); ok {
		bidMargin, err = decimal.NewFromString(bidMarginEnv)
		if err != nil {
			return nil, fmt.Errorf("error with converting %s_BM", assetCode)
		}
	}

	if safetyMarginEnv, ok := os.LookupEnv(utils.TrimSpacesFromEnv(assetCode + "_SM")); ok {
		safetyMargin, err = decimal.NewFromString(safetyMarginEnv)
		if err != nil {
			return nil, fmt.Errorf("error with converting %s_SM", assetCode)
		}
	}

	if orderBookGapToleranceEnv, ok := os.LookupEnv(utils.TrimSpacesFromEnv(assetCode + "_OBGT")); ok {
		orderBookGapTolerance, err = decimal.NewFromString(orderBookGapToleranceEnv)
		if err != nil {
			return nil, fmt.Errorf("error with converting %s_OBGT", assetCode)
		}
	}

	if maxOrderSizeEnv, ok := os.LookupEnv(utils.TrimSpacesFromEnv(assetCode + "_MAX_ORDER_SIZE")); ok {
		maxOrderSize, err = decimal.NewFromString(maxOrderSizeEnv)
		if err != nil {
			return nil, fmt.Errorf("error with converting %s_MAX_ORDER_SIZE", assetCode)
		}
	}

	//if all constraints are empty - just skip
	if askMargin.IsZero() && bidMargin.IsZero() && safetyMargin.IsZero() && orderBookGapTolerance.IsZero() && maxOrderSize.IsZero() {
		return nil, nil
	}

	return &AssetPairCfg{
		AskMargin:             askMargin,
		BidMargin:             bidMargin,
		SafetyMargin:          safetyMargin,
		OrderBookGapTolerance: orderBookGapTolerance,
		MaxOrderSize:          maxOrderSize,
	}, nil
}

// checkAssetCfgEnvVars parse asset-related env keys
// if one of the grouped vars is empty or cant be parsed we skip this asset
func checkAssetCfgEnvVars(assetCode string) (*AssetCfg, error) {
	var (
		settlementThreshold, lowThreshold, minBalance, volatilityThreshold decimal.Decimal
		err                                                                error
		volatilityPeriod                                                   time.Duration
	)

	if settlementThresholdEnv, ok := os.LookupEnv(utils.TrimSpacesFromEnv(assetCode + "_SETTLEMENT_THRESHOLD")); ok {
		settlementThreshold, err = decimal.NewFromString(settlementThresholdEnv)
		if err != nil {
			return nil, fmt.Errorf("error with converting %s_SETTLEMENT_THRESHOLD", assetCode)
		}
	}

	if lowThresholdEnv, ok := os.LookupEnv(utils.TrimSpacesFromEnv(assetCode + "_LOW_THRESHOLD")); ok {
		lowThreshold, err = decimal.NewFromString(lowThresholdEnv)
		if err != nil {
			return nil, fmt.Errorf("error with converting %s_LOW_THRESHOLD", assetCode)
		}
	}

	if minBalanceEnv, ok := os.LookupEnv(utils.TrimSpacesFromEnv(assetCode + "_MIN_BALANCE")); ok {
		minBalance, err = decimal.NewFromString(minBalanceEnv)
		if err != nil {
			return nil, fmt.Errorf("error with converting %s_MIN_BALANCE", assetCode)
		}
	}
	if volatilityThresholdEnv, ok := os.LookupEnv(utils.TrimSpacesFromEnv(assetCode + "_VOLATILITY_THRESHOLD")); ok {
		volatilityThreshold, err = decimal.NewFromString(volatilityThresholdEnv)
		if err != nil {
			return nil, fmt.Errorf("error with converting %s_VOLATILITY_THRESHOLD", assetCode)
		}
	}
	if volatilityPeriodEnv, ok := os.LookupEnv(utils.TrimSpacesFromEnv(assetCode + "_VOLATILITY_PERIOD")); ok {
		volatilityPeriod, err = time.ParseDuration(volatilityPeriodEnv)
		if err != nil {
			return nil, fmt.Errorf("error with converting %s_VOLATILITY_PERIOD", assetCode)
		}
	}

	if settlementThreshold.IsZero() && lowThreshold.IsZero() && minBalance.IsZero() &&
		volatilityPeriod == 0 && volatilityThreshold.IsZero() {
		return nil, nil
	}

	return &AssetCfg{
		SettlementThreshold: settlementThreshold,
		LowThreshold:        lowThreshold,
		MinBalance:          minBalance,
		VolatilityPeriod:    volatilityPeriod,
		VolatilityThreshold: volatilityThreshold,
	}, nil
}

func parseDoNotTradePeriods() ([]NotTradePeriod, error) {
	var doNotTradePeriods []NotTradePeriod
	timePeriodsStr := strings.Split(utils.TrimSpacesFromEnv(os.Getenv(utils.DO_NOT_TRADE_PERIODS)), ";")

	for _, p := range timePeriodsStr {
		periods := strings.Split(p, ",")
		if len(periods) > 2 {
			return nil, errors.New("more then 2 dates in period")
		}

		from, err := utils.ParseStringToTime(periods[0])
		if err != nil {
			return nil, err
		}
		to, err := utils.ParseStringToTime(periods[1])
		if err != nil {
			return nil, err
		}
		doNotTradePeriods = append(doNotTradePeriods, NotTradePeriod{
			From: from,
			To:   to,
		})
	}
	return doNotTradePeriods, nil
}

func buildTradeOnlyMarkets(marketsTradeOnly string) map[string][]string {
	if marketsTradeOnly == "" {
		return nil
	}
	res := make(map[string][]string)
	markets := strings.Split(marketsTradeOnly, ",")
	for _, m := range markets {
		res[m] = strings.Split(m, "/")
	}
	return res
}
