package config

import (
	"strings"
	"time"

	"github.com/shopspring/decimal"
)

type Sp1g0tConfig struct {
	OrganizationName      string
	Email                 string
	WalletAddress         string
	LoopPauseDuration     time.Duration
	AssetBlackList        string
	OrderHoldoffPeriod    int
	DoNotTradePeriods     []NotTradePeriod
	AskMargin             decimal.Decimal
	BidMargin             decimal.Decimal
	SafetyMargin          decimal.Decimal
	OrderBookGapTolerance decimal.Decimal
	SettlementThreshold   decimal.Decimal
	LowThreshold          decimal.Decimal
	MaxOrderSize          decimal.Decimal
	MinBalance            decimal.Decimal
	VolatilityPeriod      time.Duration
	VolatilityThreshold   decimal.Decimal
	LossPeriod            float64
	LossThreshold         decimal.Decimal
	AssetPairCfgMap       map[string]*AssetPairCfg
	AssetCfgMap           map[string]*AssetCfg
	MarketsTradeOnly      map[string][]string
}

type NotTradePeriod struct {
	From time.Time
	To   time.Time
}

//AssetPairCfg hold asset pair specific env vars (like BTC_USDT_AM, SOL_USDC_SM etc.)
type AssetPairCfg struct {
	AskMargin             decimal.Decimal
	BidMargin             decimal.Decimal
	SafetyMargin          decimal.Decimal
	OrderBookGapTolerance decimal.Decimal
	MaxOrderSize          decimal.Decimal
}

//AssetCfg hold asset specific env vars (like XXX_SETTLEMENT_THRESHOLD, XXX_MIN_BALANCE etc)
type AssetCfg struct {
	SettlementThreshold decimal.Decimal
	LowThreshold        decimal.Decimal
	MinBalance          decimal.Decimal
	VolatilityPeriod    time.Duration
	VolatilityThreshold decimal.Decimal
}

func (cfg Sp1g0tConfig) GetSafetyMarginValue(assetPair string) (res decimal.Decimal) {
	res = cfg.SafetyMargin
	if _, ok := cfg.AssetPairCfgMap[assetPair]; ok {
		if !cfg.AssetPairCfgMap[assetPair].SafetyMargin.IsZero() {
			res = cfg.AssetPairCfgMap[assetPair].SafetyMargin
		}
	}
	return
}

func (cfg Sp1g0tConfig) GetBidMarginValue(assetPair string) (res decimal.Decimal) {
	res = cfg.BidMargin
	if _, ok := cfg.AssetPairCfgMap[assetPair]; ok {
		if !cfg.AssetPairCfgMap[assetPair].BidMargin.IsZero() {
			res = cfg.AssetPairCfgMap[assetPair].BidMargin
		}
	}
	return
}

func (cfg Sp1g0tConfig) GetAskMarginValue(assetPair string) (res decimal.Decimal) {
	res = cfg.AskMargin
	if _, ok := cfg.AssetPairCfgMap[assetPair]; ok {
		if !cfg.AssetPairCfgMap[assetPair].AskMargin.IsZero() {
			res = cfg.AssetPairCfgMap[assetPair].AskMargin
		}
	}
	return
}

func (cfg Sp1g0tConfig) GetOBGTValue(assetPair string) (res decimal.Decimal) {
	res = cfg.OrderBookGapTolerance
	if _, ok := cfg.AssetPairCfgMap[assetPair]; ok {
		if !cfg.AssetPairCfgMap[assetPair].OrderBookGapTolerance.IsZero() {
			res = cfg.AssetPairCfgMap[assetPair].OrderBookGapTolerance
		}
	}
	return
}

func (cfg Sp1g0tConfig) GetSettlementTreshold(asset string) (res decimal.Decimal) {
	assetCode := strings.Split(asset, "/")[0]
	res = cfg.SettlementThreshold
	if _, ok := cfg.AssetCfgMap[assetCode]; ok {
		if !cfg.AssetCfgMap[assetCode].SettlementThreshold.IsZero() {
			res = cfg.AssetCfgMap[assetCode].SettlementThreshold
		}
	}
	return
}

func (cfg Sp1g0tConfig) GetMinBalance(asset string) (res decimal.Decimal) {
	assetCode := strings.Split(asset, "/")[0]
	res = cfg.MinBalance
	if _, ok := cfg.AssetCfgMap[assetCode]; ok {
		if !cfg.AssetCfgMap[assetCode].MinBalance.IsZero() {
			res = cfg.AssetCfgMap[assetCode].MinBalance
		}
	}
	return
}

func (cfg Sp1g0tConfig) GetMaxOrderSize(asset string) (res decimal.Decimal) {
	assetCode := strings.Split(asset, "/")[0]
	res = cfg.MaxOrderSize
	if _, ok := cfg.AssetPairCfgMap[assetCode]; ok {
		if !cfg.AssetPairCfgMap[assetCode].MaxOrderSize.IsZero() {
			res = cfg.AssetPairCfgMap[assetCode].MaxOrderSize
		}
	}
	return
}

func (cfg Sp1g0tConfig) GetLowThreshold(asset string) (res decimal.Decimal) {
	assetCode := strings.Split(asset, "/")[0]
	res = cfg.LowThreshold
	if _, ok := cfg.AssetCfgMap[assetCode]; ok {
		if !cfg.AssetCfgMap[assetCode].LowThreshold.IsZero() {
			res = cfg.AssetCfgMap[assetCode].LowThreshold
		}
	}
	return
}

func (cfg Sp1g0tConfig) GetVolatilityPeriod(asset string) (res time.Duration) {
	assetCode := strings.Split(asset, "/")[0]
	res = cfg.VolatilityPeriod
	if _, ok := cfg.AssetCfgMap[assetCode]; ok {
		if cfg.AssetCfgMap[assetCode].VolatilityPeriod != 0 {
			res = cfg.AssetCfgMap[assetCode].VolatilityPeriod
		}

	}
	return
}

func (cfg Sp1g0tConfig) GetVolatilityThreshold(asset string) (res decimal.Decimal) {
	assetCode := strings.Split(asset, "/")[0]
	res = cfg.VolatilityThreshold
	if _, ok := cfg.AssetCfgMap[assetCode]; ok {
		if !cfg.AssetCfgMap[assetCode].VolatilityThreshold.IsZero() {
			res = cfg.AssetCfgMap[assetCode].VolatilityThreshold
		}
	}
	return
}

func (cfg Sp1g0tConfig) GetLossPeriod() (res float64) {
	return cfg.LossPeriod
}

func (cfg Sp1g0tConfig) GetLossThreshold() decimal.Decimal {
	return cfg.LossThreshold
}
