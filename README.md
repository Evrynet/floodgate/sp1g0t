# sp1g0t

floodgate trading bot - uses the [floodgate API](https://gitlab.com/Evrynet/floodgate/floodgate-api).

<img src="docs/images/main-loop.png" alt="sp1g0t main loop" height="700">

# env variables
```DB_NAME=sp1g0t
DB_HOST=sp1g0t_db
DB_PORT=5432
DB_USER=user
DB_PASSWORD=password 
DB_SSL=false

ORGANIZATION_NAME=some_name12312
EMAIL=test_money@gmail.com
WALLET=
 
SOLANA_PRIVATE_KEY=
SOLANA_HOST=http://hydra:3000/floodgate/1.0.0

LOOP_PAUSE_DURATION=15s # number of seconds to pause the loop (as an advice ending should be `s` for seconds, `m` for minutes)
ORDER_HOLDOFF_PERIOD=5 # number of seconds to wait before responding to an order
ASSET_BLACK_LIST=SCUM,DOGE # comma separated list of asset names that shall NOT be traded by the bot
DO_NOT_TRADE_PERIODS=2022-02-22T14:00,2022-02-22T18:00;2022-03-21T12:00,2022-03-21T14:00;2022-03-28T09:00,2022-03-28T12:38

#global variables (used if not redefined for specific asset)
SETTLEMENT_THRESHOLD=220
LOW_THRESHOLD=1000
MAX_ORDER_SIZE=2
MIN_BALANCE=0.001
AM=5 #ask margin - defines the sell order range the bot cares about in %
BM=5 #bid margin - defines the buy order range the bot cares about in %
SM=5 #5 safety margin - to be applied by the bot when placing orders in %
     #price for bids: market price - SM%
     #price for asks: market price + SM%
OBGT=10 #order book gap tolerance in %
VOLATILITY_THRESHOLD=1 #price change percentage of an asset 
VOLATILITY_PERIOD=4h
LOSS_THRESHOLD=100
LOSS_PERIOD=1h

#asset specific
BTC_SETTLEMENT_THRESHOLD=200 # trigger the settlement for the given asset if the available balance in the open orders account exceeeds this threshold
BTC_LOW_THRESHOLD=100  # trigger an alert if the balance for the given asset falls below this threshold
BTC_MIN_BALANCE=10 # minimum balance the bot must maintain for the given asset at all times
#market specific
BTC_USDT_AM=1
BTC_USDT_BM=30
BTC_USDT_SM=3
BTC_USDT_OBGT=11
BTC_USDT_MAX_ORDER_SIZE=20.5 # max order size for the given asset

#just 3 variables would be asset/market specific, all other values would be taken from global variables above
#e.g. AM for ETH will be 5
ETH_VOLATILITY_THRESHOLD=22 #price change percentage of an asset 
ETH_VOLATILITY_PERIOD=33
ETH_USDT_MAX_ORDER_SIZE=20.5
```


# commands
```make build``` - build binary file in ```bin``` directory

```sp1g0t start``` - start bot

```sp1g0t --list-tokens``` -  show list of all unique token pairs from all markets in alphabbetical order

```sp1g0t --list-markets ``` - show list of  all markets

```sp1g0t --list-markets --basep=BTC``` - show list of markets where Base asset is BTC 

```sp1g0t --list-markets --quotep=SOL``` - show list of markets where Quote asset is SOL

```sp1g0t --list-markets --basep=BTC --quotep=SOL``` - show list of markets where Base asset is BTC and Quote asset is SOL

```sp1g0t  --csv name``` - download all markets as csv file. If name is empty - saves as 'markets.csv' to current working directory.

```sp1g0t  --register``` - register in the API

```sp1g0t  --verify --code={token}``` - verify you account with token from email

```sp1g0t order --place --pair=SOL/USDC --price=0.02 --side=sell --size=0.1 --type=limit``` - manualy place an order

```sp1g0t order --id={order_id}``` - get info about order by id (adding ```--local``` will returnn results from local db)

```sp1g0t order --cancel --id={order_id}``` - cancel order by id

```sp1g0t order --cancel --csv``` - show all own canceled orders printed on stdout in csv format

```sp1g0t order --cancel --status={open|new}``` - cancel all orders with given status

```CTRL+C``` - stop bot execution

# Docker
```docker-compose up --build``` - deploy db and bot and execute ```start``` cmd to begin trading

**NOTE**: on bot start all UP migraions will be applied automatically



## modus operandi

Once it has registered with the `hydra` API server the `sp1g0t` trading bot operates a main loop that fetches
  1. markets (asset pairs)
  1. prices for the assets traded in these markets (from external sources i.e. centralized exchanges) and "triangulates" the markets' pair prices
  1. open orders account balances for these markets
  1. open orders for these markets
  1. wallet balances for the assets that are available to the bot

Based on this data the bot will:
  - settle open order accounts where the available balances is above a configurable threshold
  - cancel orders that are
    1. obsolete
    1. unprofitable (i.e. the trade would result in a profit that is below a configurable threshold)
  - place new orders for languishing orders i.e. orders that
    1. are open and unfilled for a configurable period of time
    1. have no counter-orders

## open questions / edge cases

  1. a market M goes away i.e. is not listed by `hydra` any more


## configuration parameters

1. `ASSET_BLACK_LIST`: comma separated list of asset names that shall *_NOT_* be traded by the bot
1. `ASSET_LIST`: comma separated list of assets supported by the bot, any assets _not_ in the list will be ignored
1. `<ASSET>_SETTLEMENT_THRESHOLD`: trigger the settlement for the given asset if the available balance in the open orders account exceeeds this threshold
1. `DB_SOURCE_NAME`: a string with the `dataSourceName` for [`sql.Open()`](https://pkg.go.dev/database/sql#Open)
1. `LOOP_PAUSE_DURATION`: number of seconds to pause the loop at step 8
1. `LOW_<ASSET>_THRESHOLD`: trigger an alert if the balance for the given asset falls below this threshold
1. `MAX_<ASSET>_ORDER_SIZE`: max order size for the given asset
1. `MIN_ASK_SPREAD`: minimum ask spread in percent (i.e. the margin to add to the market price when we place an ask)
1. `MIN_<ASSET>_BALANCE`: minimum balance the bot must maintain for the given asset at all times
1. `MIN_BID_SPREAD`: minimum bid spread in percent (i.e. the margin to deduct from the market price when we place a bid)
1. `ORDER_HOLDOFF_PERIOD`: number of seconds to wait before responding to an order
1. `PRIVATE_KEY`: the private key associated with the address (public key) of the solana wallet used by the bot
1. `SMTP_AUTH_DATA`: data needed to authenticate toward an smtp server using e.g. [https://pkg.go.dev/net/smtp#Client.Auth](https://pkg.go.dev/net/smtp#Client.Auth)
1. `WALLET_ADDRESS`: the address of the solana wallet used by the bot

## general principles
1. `sp1g0t` stores all data in a relational database (configurable connection parameters)
1. every `sp1g0t` instance / deployment will have its own database; please add an `--init-db` command line argument that will initialize the database i.e. create the schema in an empty database
1. `sp1g0t` logs _all_ actions to an append-only system log (configurable endpoint) in a format that is easy to process by machines
1. if any of the asset specific parameters (settlement threshold, low balance threshold, maximum order size) are not set the bot shall
   1. print and log an error
   1. raise an alert
   1. stop trading that asset
