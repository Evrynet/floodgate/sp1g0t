#first stage - builder
FROM golang:1.17 as builder

WORKDIR /sp1g0t
COPY . /sp1g0t

ENV GO111MODULE=on

RUN CGO_ENABLED=0 GOOS=linux go build -o ./bin/sp1g0t .

#second stage
FROM alpine:latest
WORKDIR /app
COPY --from=builder ./sp1g0t/bin/sp1g0t  /app
ENTRYPOINT [ "./sp1g0t", "start" ] 

