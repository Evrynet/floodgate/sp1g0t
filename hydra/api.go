package hydra

import (
	"fmt"
	"net/http"

	"gitlab.com/Evrynet/sp1g0t/models"
)

// Register register new user and get auth token for future requests
func (h *Hydra) Register(req models.RegistrationReq) (err error) {
	return h.MakeRequest(http.MethodPost, "/register", req, nil)
}

func (h *Hydra) Verify(token string) (err error) {
	return h.MakeRequest(http.MethodPost, "/register/verify", TokenVerify{Token: token}, nil)
}

func (h *Hydra) GetMarkets(basep, quotep string) (marketPairs models.MarketPairs, err error) {
	marketsURL := "/markets"
	switch {
	case basep != "" && quotep == "":
		marketsURL = fmt.Sprintf("%s?basep=%s", marketsURL, basep)
	case basep == "" && quotep != "":
		marketsURL = fmt.Sprintf("%s?quotep=%s", marketsURL, quotep)
	default:
		marketsURL = fmt.Sprintf("%s?basep=%s&quotep=%s", marketsURL, basep, quotep)
	}

	err = h.MakeRequest(http.MethodGet, marketsURL, nil, &marketPairs)
	return marketPairs, err
}

func (h *Hydra) GetMarketPairData(pair string) (marketResult models.MarketResult, err error) {
	err = h.MakeRequest(http.MethodGet, fmt.Sprintf("/markets/%s", pair), nil, &marketResult)
	return marketResult, err
}

func (h *Hydra) GetOrders() (orders []models.OrderResult, err error) {
	err = h.MakeRequest(http.MethodGet, "/orders", nil, &orders)
	return orders, err
}

func (h *Hydra) GetOrdersForMarket(marketAddress string) (orders models.OrderResult, err error) {
	err = h.MakeRequest(http.MethodGet, fmt.Sprintf("/markets/%s/orders", marketAddress), nil, &orders)
	return orders, err
}

func (h *Hydra) GetOrder(id string) (order models.OrderResult, err error) {
	err = h.MakeRequest(http.MethodGet, fmt.Sprintf("/orders/%s", id), nil, &order)
	return order, err
}

func (h *Hydra) PlaceOrder(req models.OrderReq) (tx models.SignatureReq, err error) {
	err = h.MakeRequest(http.MethodPost, "/orders", req, &tx)
	return tx, err
}

func (h *Hydra) CancelAllOrders() (tx models.SignatureReq, err error) {
	err = h.MakeRequest(http.MethodDelete, "/orders", nil, &tx)
	return tx, err
}

func (h *Hydra) CancelOrder(id string) (tx models.SignatureReq, err error) {
	err = h.MakeRequest(http.MethodDelete, fmt.Sprintf("/orders/%s", id), nil, &tx)
	return tx, err
}

func (h *Hydra) Sign(txID string, signatureData models.SignatureData) (res models.SignedOrders, err error) {
	err = h.MakeRequest(http.MethodPost, fmt.Sprintf("/tx/%s/sign", txID), signatureData, &res)
	return res, err
}

func (h *Hydra) Settle(market, marketAddress string) (tx models.SignatureReq, err error) {
	err = h.MakeRequest(http.MethodPost, "/settle", struct {
		Market        string `json:"market"`
		MarketAddress string `json:"market_address"`
	}{
		Market:        market,
		MarketAddress: marketAddress,
	}, &tx)
	return tx, err
}

func (h *Hydra) GetWalletBalances() (balances *models.WalletBalancesResult, err error) {
	err = h.MakeRequest(http.MethodGet, "/wallet/balances", nil, &balances)
	return
}

func (h *Hydra) GetMarketBalances(marketAddress string) (balances *models.OpenBalancesResult, err error) {
	err = h.MakeRequest(http.MethodGet, fmt.Sprintf("/markets/%s/balances", marketAddress), nil, &balances)
	return
}

func (h *Hydra) GetOrderBook(marketAddress string) (orderbook *models.OrderbookResult, err error) {
	err = h.MakeRequest(http.MethodGet, fmt.Sprintf("/markets/%s/orderbook", marketAddress), nil, &orderbook)
	return
}

func (h *Hydra) GetTheLatestTrade(marketAddress string) (trades *models.LatestTradesResult, err error) {
	err = h.MakeRequest(http.MethodGet, fmt.Sprintf("/markets/%s/latest?N=1", marketAddress), nil, &trades)
	return
}

var _ HydraInterface = (*Hydra)(nil)
