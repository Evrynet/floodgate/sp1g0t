package hydra

import (
	"errors"

	"github.com/stretchr/testify/mock"
	"gitlab.com/Evrynet/sp1g0t/models"
)

type HydraMock struct {
	mock.Mock
}

func (hm *HydraMock) Register(req models.RegistrationReq) (err error) {
	return errors.New("not implemented in mock")
}

func (hm *HydraMock) Verify(token string) (err error) {
	return errors.New("not implemented in mock")
}

func (hm *HydraMock) GetMarkets(basep, quotep string) (marketPairs models.MarketPairs, err error) {
	if basep == "BTC" && quotep == "USDT" {
		return models.MarketPairs{
			Success: true,
			Pairs:   []string{"BTC/USDT"},
		}, nil
	}
	if basep == "SOL" && quotep == "USDT" {
		return models.MarketPairs{
			Success: true,
			Pairs:   []string{"SOL/USDT"},
		}, nil
	}

	return models.MarketPairs{
		Success: true,
		Pairs:   []string{"BTC/USDT", "SOL/USDT"},
	}, nil
}
func (hm *HydraMock) GetMarketPairData(pair string) (marketResult models.MarketResult, err error) {
	switch pair {
	case "BTCUSDT":
		return models.MarketResult{
			Success: true,
			Market: models.MarketData{
				Name:         "BTC/USDT",
				BaseMintAddr: "btc_base_mint_address",
			},
		}, nil
	case "SOLUSDT":
		return models.MarketResult{
			Success: true,
			Market: models.MarketData{
				Name:         "SOL/USDT",
				BaseMintAddr: "sol_base_mint_address",
			},
		}, nil
	case "ETHUSDT":
		return models.MarketResult{
			Success: true,
			Market: models.MarketData{
				Name:         "ETH/USDT",
				BaseMintAddr: "eth_base_mint_address",
			},
		}, nil
	default:
		return models.MarketResult{}, errors.New("not implemented in mock")
	}

}
func (hm *HydraMock) GetOrders() (orders []models.OrderResult, err error) {
	return nil, errors.New("not implemented in mock")
}
func (hm *HydraMock) GetOrdersForMarket(marketAddress string) (orders models.OrderResult, err error) {
	return models.OrderResult{
		Orders: []models.OrderData{
			{
				Id:            "id1",
				ClientId:      "test_client",
				MarketAddress: "address1",
				Price:         "1050",
				Side:          "sell",
				Size:          "2",
				Market:        "BTC/USDT",
			},
			{
				Id:            "id2",
				ClientId:      "test_client",
				MarketAddress: "address1",
				Price:         "800",
				Side:          "sell",
				Size:          "2",
				Market:        "BTC/USDT",
			},
			{
				Id:            "id3",
				ClientId:      "test_client",
				MarketAddress: "address1",
				Price:         "1070",
				Side:          "sell",
				Size:          "3",
				Market:        "BTC/USDT",
			},
			{
				Id:            "id11",
				ClientId:      "test_client",
				MarketAddress: "address1",
				Price:         "990",
				Side:          "buy",
				Size:          "5",
				Market:        "BTC/USDT",
			},
			{
				Id:            "id22",
				ClientId:      "test_client",
				MarketAddress: "address1",
				Price:         "800",
				Side:          "buy",
				Size:          "20",
				Market:        "BTC/USDT",
			},
			{
				Id:            "id4",
				ClientId:      "client1",
				MarketAddress: "address1",
				Price:         "4",
				Side:          "sell",
				Size:          "2",
				Market:        "BTC/USDT",
			},
			{
				Id:            "id5",
				ClientId:      "client1",
				MarketAddress: "address1",
				Price:         "3",
				Side:          "sell",
				Size:          "1",
				Market:        "BTC/USDT",
			},
		},
	}, nil
}
func (hm *HydraMock) GetOrder(id string) (order models.OrderResult, err error) {
	return models.OrderResult{}, errors.New("not implemented in mock")
}
func (hm *HydraMock) PlaceOrder(req models.OrderReq) (tx models.SignatureReq, err error) {
	return models.SignatureReq{}, errors.New("not implemented in mock")
}
func (hm *HydraMock) CancelAllOrders() (tx models.SignatureReq, err error) {
	return models.SignatureReq{}, errors.New("not implemented in mock")
}
func (hm *HydraMock) CancelOrder(id string) (tx models.SignatureReq, err error) {
	return models.SignatureReq{
		TxId:   "txid",
		TxData: "txdata",
	}, nil
}
func (hm *HydraMock) Sign(txID string, signatureData models.SignatureData) (res models.SignedOrders, err error) {
	return models.SignedOrders{
		Result: models.Result{
			Success: true,
		},
	}, nil
}

func (hm *HydraMock) SignBody(payload string) string {
	return "not implemented"
}

func (hm *HydraMock) Settle(market, marketAddress string) (tx models.SignatureReq, err error) {
	return models.SignatureReq{
		TxId:   "txid",
		TxData: "txdata",
	}, nil
}

func (hm *HydraMock) GetWalletBalances() (balances *models.WalletBalancesResult, err error) {
	return &models.WalletBalancesResult{
		Success: true,
		Balances: []models.BalanceData{
			{
				Asset:    "RAY",
				MintAddr: "some_mint_addr",
				Total:    "100",
			},
			{
				Asset:    "BTC",
				MintAddr: "some_mint_addr2",
				Total:    "999",
			},
		},
	}, nil
}

func (hm *HydraMock) GetMarketBalances(marketAddress string) (balances *models.OpenBalancesResult, err error) {
	return nil, errors.New("not implemented in mock")
}

func (hm *HydraMock) GetOrderBook(marketAddress string) (orderbook *models.OrderbookResult, err error) {
	return &models.OrderbookResult{
		Success: true,
		Asks: []models.OrderbookData{
			{
				Price: "1050",
				Size:  "2",
			},
			{
				Price: "800",
				Size:  "2",
			},
			{
				Price: "1070",
				Size:  "3",
			},
			{
				Price: "1020",
				Size:  "2",
			},
			{
				Price: "1030",
				Size:  "1",
			},
		},
		Bids: []models.OrderbookData{
			{
				Price: "990",
				Size:  "5",
			},
			{
				Price: "800",
				Size:  "20",
			},
		},
	}, nil
}

func (hm *HydraMock) GetTheLatestTrade(marketAddress string) (trades *models.LatestTradesResult, err error) {
	if marketAddress == "solana_address" {
		return &models.LatestTradesResult{
			Success: true,
			Trades: []models.TradeData{
				{
					MarketAddress: "solana_address",
					Market:        "SOL/USDT",
					Side:          "buy",
					Price:         "170",
				},
			},
		}, nil
	}
	if marketAddress == "bitcoin_address" {
		return &models.LatestTradesResult{
			Success: true,
			Trades: []models.TradeData{
				{
					MarketAddress: "bitcoin_address",
					Market:        "BTC/USDT",
					Side:          "buy",
					Price:         "11300",
				},
			},
		}, nil
	}
	return nil, errors.New("unknown market address")
}

var _ HydraInterface = (*HydraMock)(nil)
