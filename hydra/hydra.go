package hydra

import "gitlab.com/Evrynet/sp1g0t/models"

//HydraInterface is interface to access hydra functionality
type HydraInterface interface {
	Register(req models.RegistrationReq) (err error)
	Verify(token string) (err error)
	GetMarkets(basep, quotep string) (marketPairs models.MarketPairs, err error)
	GetMarketPairData(pair string) (marketResult models.MarketResult, err error)
	GetOrders() (orders []models.OrderResult, err error)
	GetOrdersForMarket(marketAddress string) (orders models.OrderResult, err error)
	GetOrder(id string) (order models.OrderResult, err error)
	PlaceOrder(req models.OrderReq) (tx models.SignatureReq, err error)
	CancelAllOrders() (tx models.SignatureReq, err error)
	CancelOrder(id string) (tx models.SignatureReq, err error)
	Sign(txID string, signatureData models.SignatureData) (res models.SignedOrders, err error)
	SignBody(payload string) string
	Settle(market, marketAddress string) (tx models.SignatureReq, err error)
	GetWalletBalances() (balances *models.WalletBalancesResult, err error)
	GetMarketBalances(marketAddress string) (balances *models.OpenBalancesResult, err error)
	GetOrderBook(marketAddress string) (orderbook *models.OrderbookResult, err error)
	GetTheLatestTrade(marketAddress string) (trades *models.LatestTradesResult, err error)
}
