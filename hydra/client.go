package hydra

import (
	"encoding/json"
	"errors"
	"net/http"
	"time"

	"gitlab.com/Evrynet/sp1g0t/models"
)

// Hydra is client for communication with hydra api
type Hydra struct {
	client     *http.Client
	host       string
	privateKey string
	publicKey  string
}

// NewHydra create new instance of Hydra client
func NewHydra(timeout int, host, privateKey, publicKey string) Hydra {
	return Hydra{
		client:     &http.Client{Timeout: time.Duration(timeout) * time.Second},
		host:       host,
		privateKey: privateKey,
		publicKey:  publicKey,
	}
}

func (h *Hydra) MakeRequest(method, path string, in, out interface{}) (err error) {
	var body []byte
	if in != nil {
		body, err = json.Marshal(in)
		if err != nil {
			return err
		}
	}

	preparedRequest, err := h.signReq(path, method, body)
	if err != nil {
		return err
	}
	preparedRequest.Close = true

	resp, err := h.client.Do(preparedRequest)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// for debug purposes
	// if path == "/wallet/balances" {
	// 	bodyBytes, err := io.ReadAll(resp.Body)
	// 	if err != nil {
	// 		logrus.Error(err)
	// 	}
	// 	bodyString := string(bodyBytes)
	// 	logrus.Debug(bodyString)
	// 	resp.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
	// }

	if resp.StatusCode != http.StatusOK && resp.StatusCode != http.StatusCreated {
		var hydraAPIError models.ModelError
		err = json.NewDecoder(resp.Body).Decode(&hydraAPIError)
		if err != nil {
			return err
		}
		return errors.New(hydraAPIError.Message)
	}

	if out != nil {
		err = json.NewDecoder(resp.Body).Decode(&out)
		if err != nil {
			return err
		}

	}

	return nil
}
