package providers

import (
	"github.com/shopspring/decimal"
	"gitlab.com/Evrynet/sp1g0t/providers/coingecko"
	"gitlab.com/Evrynet/sp1g0t/providers/gateio"
)

type PriceProvider struct {
	Coingecko Provider
	GateIO    Provider
}

func NewPriceProvider(coingecko *coingecko.Coingecko, gateIO *gateio.GateIO) *PriceProvider {
	return &PriceProvider{
		Coingecko: coingecko,
		GateIO:    gateIO,
	}
}

type Provider interface {
	GetUsdPrice(asset string) (decimal.Decimal, error)
}
