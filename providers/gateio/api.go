package gateio

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/Evrynet/sp1g0t/utils"
)

type GateeIOApiInterface interface {
	getCurrencyPair(pair string) ([]CurrencyPairResponse, error)
}

type GateeIOApi struct{}

type CurrencyPairResponse struct {
	CurrencyPair string `json:"currency_pair"`
	Last         string `json:"last"`
}

type GateIoErr struct {
	Label   string `json:"label"`
	Message string `json:"message"`
}

func (g GateIoErr) Error() string {
	return g.Message
}

//curl https://api.gateio.ws/api/v4/spot/tickers?currency_pair=EVRY_USDT
//Retrieve ticker information
//Return only related data if currency_pair is specified; otherwise return all of them
func (g *GateeIOApi) getCurrencyPair(pair string) ([]CurrencyPairResponse, error) {
	var (
		currencyPairResponse []CurrencyPairResponse
		gateIoErr            *GateIoErr
	)

	url := fmt.Sprintf("%s/spot/tickers?currency_pair=%s", utils.GATEIO_API, pair)

	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 300 {
		if err := json.NewDecoder(resp.Body).Decode(&gateIoErr); err != nil {
			return nil, err
		}
		if gateIoErr != nil {
			return nil, gateIoErr
		}
	}

	if err := json.NewDecoder(resp.Body).Decode(&currencyPairResponse); err != nil {
		return nil, err
	}
	return currencyPairResponse, nil
}
