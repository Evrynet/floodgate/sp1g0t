package gateio

import (
	"fmt"
	"strings"

	"github.com/shopspring/decimal"
)

type GateIO struct {
	api GateeIOApiInterface
}

func NewGateIO(api GateeIOApiInterface) *GateIO {
	return &GateIO{
		api: api,
	}
}

const usd = "USD"

func (g *GateIO) GetUsdPrice(asset string) (decimal.Decimal, error) {
	pair := fmt.Sprintf("%s_%s", strings.ToUpper(asset), usd)
	currencyPair, err := g.api.getCurrencyPair(pair)
	if err != nil {
		return decimal.Decimal{}, err
	}

	price, err := decimal.NewFromString(currencyPair[0].Last)
	if err != nil {
		return decimal.Decimal{}, err
	}
	return price, nil
}
