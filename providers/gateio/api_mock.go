package gateio

import "github.com/stretchr/testify/mock"

type GateeIOApiMock struct {
	mock.Mock
}

func (g GateeIOApiMock) getCurrencyPair(pair string) ([]CurrencyPairResponse, error) {
	if pair == "BTC_USDT" {
		return []CurrencyPairResponse{
			{
				CurrencyPair: "BTC_USDT",
				Last:         "12345",
			},
		}, nil
	}
	if pair == "BTC_USD" {
		return []CurrencyPairResponse{
			{
				CurrencyPair: "BTC_USDT",
				Last:         "12345",
			},
		}, nil
	}
	if pair == "SOL_USDT" {
		return []CurrencyPairResponse{
			{
				CurrencyPair: "SOL_USDT",
				Last:         "200",
			},
		}, nil
	}
	if pair == "SOL_USD" {
		return []CurrencyPairResponse{
			{
				CurrencyPair: "SOL_USDT",
				Last:         "200",
			},
		}, nil
	}
	if pair == "RAY_USDT" {
		return []CurrencyPairResponse{
			{
				CurrencyPair: "RAY_USDT",
				Last:         "50",
			},
		}, nil
	}
	if pair == "RAY_USD" {
		return []CurrencyPairResponse{
			{
				CurrencyPair: "RAY_USDT",
				Last:         "50",
			},
		}, nil
	}
	if pair == "USDT_USD" {
		return []CurrencyPairResponse{
			{
				CurrencyPair: "USDT_USD",
				Last:         "1",
			},
		}, nil
	}
	return nil, nil
}
