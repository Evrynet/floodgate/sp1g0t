package coingecko

import (
	"errors"

	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/mock"
)

type CoingeckoApiMock struct {
	mock.Mock
}

func (m *CoingeckoApiMock) getCoinsList() ([]listResponse, error) {
	return []listResponse{
		{
			ID:     "bitconnect",
			Symbol: "bcc",
			Name:   "Bitconnect",
		},
		{
			ID:     "bitconnectx-genesis",
			Symbol: "bccx",
			Name:   "BCCXGenesis",
		},
		{

			ID:     "classicbitcoin",
			Symbol: "cbtc",
			Name:   "ClassicBitcoin",
		},
		{
			ID:     "improved-bitcoin",
			Symbol: "iBTC",
			Name:   "Improved Bitcoin",
		},
		{
			ID:     "bitcoin",
			Symbol: "btc",
			Name:   "Bitcoin",
		},
		{
			ID:     "solana",
			Symbol: "sol",
			Name:   "Solana",
		},
		{
			ID:     "ray",
			Symbol: "ray",
			Name:   "Raydium",
		},
		{
			ID:     "usdt",
			Symbol: "usdt",
			Name:   "usdt",
		},
	}, nil
}

func (m *CoingeckoApiMock) getCoinUsdPrice(asset string) (decimal.Decimal, error) {
	if asset == "bitcoin" {
		return decimal.NewFromInt(10000), nil
	}
	if asset == "evrynet" {
		return decimal.NewFromInt(100), nil
	}
	if asset == "solana" {
		return decimal.NewFromInt(110), nil
	}
	if asset == "ray" {
		return decimal.NewFromInt(55), nil
	}
	if asset == "usdt" {
		return decimal.NewFromInt(1), nil
	}
	return decimal.Decimal{}, errors.New("wrong test asset")
}
