package coingecko

import (
	"fmt"
	"math/rand"
	"sort"
	"strings"
	"time"

	"github.com/shopspring/decimal"
)

type Coingecko struct {
	api       CoingeckoApiInterface
	isTimeout bool
}

func NewCoingecko(api CoingeckoApiInterface, isTimeout bool) *Coingecko {
	return &Coingecko{
		api:       api,
		isTimeout: isTimeout,
	}
}

const usd = "usd"

func (c *Coingecko) GetUsdPrice(asset string) (decimal.Decimal, error) {
	if c.isTimeout {
		timeout()
	}
	list, err := c.api.getCoinsList()
	if err != nil {
		return decimal.Decimal{}, err
	}

	var tokenIDs []string
	for _, v := range list {
		//currency names are lowercased in gecko api
		if v.Symbol == strings.ToLower(asset) {
			tokenIDs = append(tokenIDs, v.ID)
		}
	}

	//coingecko returns deffierent ids with same symbol, e.g. for ETH - ethereum-wormhole,ethereum, for BNB - binance-coin-wormhole, binancecoin,oec-binance-coin
	//and we need the shortest one as the most suitable
	shortestCurrencyID := getShortestID(tokenIDs)

	if shortestCurrencyID != "" {
		if c.isTimeout {
			timeout()
		}
		price, err := c.api.getCoinUsdPrice(shortestCurrencyID)
		if err != nil {
			return decimal.Decimal{}, err
		}
		return price, nil
	}

	return decimal.Decimal{}, fmt.Errorf("can`t get coingecko price for %s", asset)
}

func getShortestID(tokenIDs []string) string {
	if len(tokenIDs) != 0 {
		sort.Slice(tokenIDs, func(i, j int) bool {
			return len(tokenIDs[i]) < len(tokenIDs[j])
		})

		return tokenIDs[0]
	}
	return ""
}

func timeout() {
	rand.Seed(time.Now().UnixNano())
	min := 2000
	max := 4000
	rnd := rand.Intn(max-min+1) + min
	time.Sleep(time.Duration(rnd * int(time.Millisecond)))
}
