package coingecko

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"github.com/shopspring/decimal"
	"gitlab.com/Evrynet/sp1g0t/utils"
)

type CoingeckoApiInterface interface {
	getCoinsList() ([]listResponse, error)
	getCoinUsdPrice(asset string) (decimal.Decimal, error)
}

type CoingeckoApi struct{}

// listResponse represents return object after 'coins/list' call
type listResponse struct {
	ID     string `json:"id"`
	Symbol string `json:"symbol"`
	Name   string `json:"name"`
}

//coinPrice represents current price of  cryptocurrency in any other supported currencies that you need.
//NOTE: Looks awfull, but return object doesnt have stable fields
//TODO: think about prettifying
var coinPrice map[string]map[string]decimal.Decimal

//https://api.coingecko.com/api/v3/coins/list
//List all supported coins id, name and symbol (no pagination required)
func (c *CoingeckoApi) getCoinsList() ([]listResponse, error) {
	var res []listResponse

	url := fmt.Sprintf("%s/coins/list", utils.COINGECKO_API)

	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusTooManyRequests {
		return nil, errors.New("too many requests for get_coins_list")
	}

	if err := json.NewDecoder(resp.Body).Decode(&res); err != nil {
		return nil, err
	}
	return res, nil
}

//curl 'https://api.coingecko.com/api/v3/simple/price?ids=evrynet&vs_currencies=usd'
func (c *CoingeckoApi) getCoinUsdPrice(asset string) (decimal.Decimal, error) {
	coinPrice = make(map[string]map[string]decimal.Decimal)
	url := fmt.Sprintf("%s/simple/price?ids=%s&vs_currencies=%s", utils.COINGECKO_API, asset, usd)

	resp, err := http.Get(url)
	if err != nil {
		return decimal.Decimal{}, err
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusTooManyRequests {
		return decimal.Decimal{}, errors.New("too many requests for get_coin_usd_price")
	}

	if err := json.NewDecoder(resp.Body).Decode(&coinPrice); err != nil {
		return decimal.Decimal{}, err
	}

	return coinPrice[asset][usd], nil
}
