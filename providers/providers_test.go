package providers

import (
	"testing"

	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/suite"
	"gitlab.com/Evrynet/sp1g0t/providers/coingecko"
	"gitlab.com/Evrynet/sp1g0t/providers/gateio"
)

type ProvidersTestSuite struct {
	suite.Suite
	provider *PriceProvider
}

func (suite *ProvidersTestSuite) SetupTest() {
	coingeckoMock := coingecko.NewCoingecko(&coingecko.CoingeckoApiMock{}, false)
	gateIOMock := gateio.NewGateIO(gateio.GateeIOApiMock{})
	suite.provider = NewPriceProvider(coingeckoMock, gateIOMock)

}

func (suite *ProvidersTestSuite) Test_Coingecko_GetUsdPrice() {
	price, err := suite.provider.Coingecko.GetUsdPrice("btc")
	suite.Nil(err)
	suite.Equal(decimal.NewFromInt(10000), price)
}

func (suite *ProvidersTestSuite) Test_GateIO_GetUsdPrice() {
	price, err := suite.provider.GateIO.GetUsdPrice("btc")
	suite.Nil(err)
	suite.Equal(decimal.NewFromInt(12345), price)
}

func TestProvidersTestSuite(t *testing.T) {
	suite.Run(t, new(ProvidersTestSuite))
}
